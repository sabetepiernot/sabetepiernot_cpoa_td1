package methode;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Abonnement extends Connexion {
	public void add(int id_cl, int id_revue, String date_deb, String date_fin) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("INSERT INTO Abonnement (id_client, id_revue, date_debut, date_fin) VALUES ('" + id_cl + "','" + id_revue + "',str_to_date('" + date_deb + "', '%d/%m/%Y'),str_to_date('" + date_fin + "', '%d/%m/%Y'));");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void edit(int id_cl, int id_revue, String date_deb, String date_fin) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate(
					"UPDATE Abonnement SET date_debut = str_to_date('" + date_deb + "','%d/%m/%Y'), date_fin = str_to_date('" + date_fin + "','%d/%m/%Y') WHERE id_client = '" + id_cl + "' AND id_revue = '" + id_revue + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void delete(int id_cl, int id_revue) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("DELETE FROM Abonnement WHERE id_client = '" + id_cl + "' AND id_revue = '" + id_revue + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void print() {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			ResultSet res = requete
					.executeQuery("SELECT id_client, id_revue, date_debut, date_fin FROM Abonnement ORDER BY id_client, id_revue;");

			while (res.next()) {
				int no = res.getInt(1);
				int no2 = res.getInt(2);
				String date_debut = res.getString("date_debut");
				String date_fin = res.getString("date_fin");
				System.out.println(no + " " + no2 + " " + date_debut + " " + date_fin);
			}

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}
}