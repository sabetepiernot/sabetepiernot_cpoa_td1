package methode;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Client extends Connexion {
	public void add(String nom, String prenom, String no_rue, String voie, String cp, String ville, String pays) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("INSERT INTO Client (nom, prenom, no_rue, voie, code_postal, ville, pays) VALUES ('" + nom + "','" + prenom + "','" + no_rue + "','" + voie + "','" + cp + "','" + ville + "','" + pays + "');");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void edit(int id, String nom, String prenom, String no_rue, String voie, String cp, String ville, String pays) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate(
					"UPDATE Client SET nom = '" + nom + "', prenom = '" + prenom + "', no_rue = '" + no_rue + "', voie = '" + voie + "', code_postal = '" + cp + "', ville = '" + ville + "', pays = '" + pays + "' WHERE id_client = '" + id + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void delete(int id) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("DELETE FROM Client WHERE id_client = '" + id + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void print() {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			ResultSet res = requete
					.executeQuery("SELECT id_client, nom, prenom, no_rue, voie, code_postal, ville, pays FROM Client ORDER BY id_client;");

			while (res.next()) {
				int no = res.getInt(1);
				String nom = res.getString("nom");
				String prenom = res.getString("prenom");
				String no_rue = res.getString("no_rue");
				String voie = res.getString("voie");
				String code_postal = res.getString("code_postal");
				String ville = res.getString("ville");
				String pays = res.getString("pays");
				System.out.println(no + " " + nom + " " + prenom + " " + no_rue + " " + voie + " " + code_postal + " " + ville + " " + pays);
			}

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}
}