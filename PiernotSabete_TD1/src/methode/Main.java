package methode;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Revue r = new Revue();
		Abonnement a = new Abonnement();
		Client c = new Client();
		Periodicite p = new Periodicite();
		
		Scanner sc = new Scanner(System.in);
		String videur = "";
		int continuer = 1;
		int table = 0;
		int action = 0;
		int action_continuer = 1;
		
		int id_client = 0;
		int id_revue = 0;
		String date_debut = "";
		String date_fin = "";
		String nom = "";
		String prenom = "";
		String no_rue = "";
		String voie = "";
		String code_postal = "";
		String ville = "";
		String pays = "";
		int id_periodicite = 0;
		String libelle = "";
		String titre = "";
		String description = "";
		String visuel = "";
		double tarif_numero = 0;
		
		while (continuer == 1) {
			do {
			System.out.println("Sur quel tableau voulez-vous effectuer une action :");
			System.out.println("(Abonnement : 1; Client : 2; Periodicite : 3; Revue : 4) ?");
			table = sc.nextInt();
			} while (table != 1 && table != 2 && table != 3 && table != 4);
			if (table == 1) {
				do {
					do {
						System.out.println("Quelle action voulez-vous realiser :");
						System.out.println("(Ajouter une ligne : 1; Modifier une ligne : 2; Supprimer une ligne : 3; Afficher la table : 4) ?");
						action = sc.nextInt();
					} while (action != 1 && action != 2 && action != 3 && action != 4);
					if (action == 1) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_client : ");
						id_client = sc.nextInt();
						System.out.println("id_revue : ");
						id_revue = sc.nextInt();
						videur = sc.nextLine();
						System.out.println("date_debut (jj/mm/aaaa) : ");
						date_debut = sc.nextLine();
						System.out.println("date_fin (jj/mm/aaaa) : ");
						date_fin = sc.nextLine();
						
						a.add(id_client, id_revue, date_debut, date_fin);
					}
					else if (action == 2) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_client : ");
						id_client = sc.nextInt();
						System.out.println("id_revue : ");
						id_revue = sc.nextInt();
						videur = sc.nextLine();
						System.out.println("date_debut (jj/mm/aaaa) : ");
						date_debut = sc.nextLine();
						System.out.println("date_fin (jj/mm/aaaa) : ");
						date_fin = sc.nextLine();
						
						a.edit(id_client, id_revue, date_debut, date_fin);
					}
					else if (action == 3) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_client : ");
						id_client = sc.nextInt();
						System.out.println("id_revue : ");
						id_revue = sc.nextInt();
						
						a.delete(id_client, id_revue);
					}
					else if (action == 4) {
						a.print();
					}
					do {
						System.out.println("Continuer les action sur cette table :");
						System.out.println("(Oui : 1; Non : 2) :");
						action_continuer = sc.nextInt();
					} while (action_continuer != 1 && action_continuer != 2);
				} while(action_continuer == 1);
			}
			else if (table == 2) {
				do {
					do {
						System.out.println("Quelle action voulez-vous realiser :");
						System.out.println("(Ajouter une ligne : 1; Modifier une ligne : 2; Supprimer une ligne : 3; Afficher la table : 4) ?");
						action = sc.nextInt();
					} while (action != 1 && action != 2 && action != 3 && action != 4);
					videur = sc.nextLine();
					if (action == 1) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("nom : ");
						nom = sc.nextLine();
						System.out.println("prenom : ");
						prenom = sc.nextLine();
						System.out.println("no_rue : ");
						no_rue = sc.nextLine();
						System.out.println("voie : ");
						voie = sc.nextLine();
						System.out.println("code_postal : ");
						code_postal = sc.nextLine();
						System.out.println("ville : ");
						ville = sc.nextLine();
						System.out.println("pays : ");
						pays = sc.nextLine();
						
						c.add(nom, prenom, no_rue, voie, code_postal, ville, pays);
					}
					else if (action == 2) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_client : ");
						id_client = sc.nextInt();
						videur = sc.nextLine();
						System.out.println("nom : ");
						nom = sc.nextLine();
						System.out.println("prenom : ");
						prenom = sc.nextLine();
						System.out.println("no_rue : ");
						no_rue = sc.nextLine();
						System.out.println("voie : ");
						voie = sc.nextLine();
						System.out.println("code_postal : ");
						code_postal = sc.nextLine();
						System.out.println("ville : ");
						ville = sc.nextLine();
						System.out.println("pays : ");
						pays = sc.nextLine();
						
						c.edit(id_client, nom, prenom, no_rue, voie, code_postal, ville, pays);
					}
					else if (action == 3) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_client : ");
						id_client = sc.nextInt();
						
						c.delete(id_client);
					}
					else if (action == 4) {
						c.print();
					}
					do {
						System.out.println("Continuer les action sur cette table :");
						System.out.println("(Oui : 1; Non : 2) :");
						action_continuer = sc.nextInt();
					} while (action_continuer != 1 && action_continuer != 2);
				} while(action_continuer == 1);
			}
			else if (table == 3) {
				do {
					do {
						System.out.println("Quelle action voulez-vous realiser :");
						System.out.println("(Ajouter une ligne : 1; Modifier une ligne : 2; Supprimer une ligne : 3; Afficher la table : 4) ?");
						action = sc.nextInt();
					} while (action != 1 && action != 2 && action != 3 && action != 4);
					if (action == 1) {
						System.out.println("Veuillez saisir les parametres requis :");
						videur = sc.nextLine();
						System.out.println("libelle : ");
						libelle = sc.nextLine();
						
						p.add(libelle);
					}
					else if (action == 2) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_periodicite : ");
						id_periodicite = sc.nextInt();
						videur = sc.nextLine();
						System.out.println("libelle : ");
						libelle = sc.nextLine();
						
						p.edit(id_periodicite, libelle);
					}
					else if (action == 3) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_periodicite : ");
						id_periodicite = sc.nextInt();
						
						p.delete(id_periodicite);
					}
					else if (action == 4) {
						p.print();
					}
					do {
						System.out.println("Continuer les action sur cette table :");
						System.out.println("(Oui : 1; Non : 2) :");
						action_continuer = sc.nextInt();
					} while (action_continuer != 1 && action_continuer != 2);
				} while(action_continuer == 1);
			}
			else if (table == 4) {
				do {
					do {
						System.out.println("Quelle action voulez-vous realiser :");
						System.out.println("(Ajouter une ligne : 1; Modifier une ligne : 2; Supprimer une ligne : 3; Afficher la table : 4) ?");
						action = sc.nextInt();
					} while (action != 1 && action != 2 && action != 3 && action != 4);
					if (action == 1) {
						System.out.println("Veuillez saisir les parametres requis :");
						videur = sc.nextLine();
						System.out.println("titre : ");
						titre = sc.nextLine();
						System.out.println("description : ");
						description = sc.nextLine();
						System.out.println("tarif_numero : ");
						tarif_numero = sc.nextDouble();
						videur = sc.nextLine();
						System.out.println("visuel : ");
						visuel = sc.nextLine();
						System.out.println("id_periodicite : ");
						id_periodicite = sc.nextInt();
						
						r.add(titre, description, (float) tarif_numero, visuel, id_periodicite);
					}
					else if (action == 2) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_revue : ");
						id_revue = sc.nextInt();
						videur = sc.nextLine();
						System.out.println("titre : ");
						titre = sc.nextLine();
						System.out.println("description : ");
						description = sc.nextLine();
						System.out.println("tarif_numero : ");
						tarif_numero = sc.nextDouble();
						videur = sc.nextLine();
						System.out.println("visuel : ");
						visuel = sc.nextLine();
						System.out.println("id_periodicite : ");
						id_periodicite = sc.nextInt();
						
						r.edit(id_revue, titre, description, (float) tarif_numero, visuel, id_periodicite);
					}
					else if (action == 3) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_revue : ");
						id_revue = sc.nextInt();
						
						r.delete(id_revue);
					}
					else if (action == 4) {
						r.print();
					}
					do {
						System.out.println("Continuer les action sur cette table :");
						System.out.println("(Oui : 1; Non : 2) :");
						action_continuer = sc.nextInt();
					} while (action_continuer != 1 && action_continuer != 2);
				} while(action_continuer == 1);
			}
			System.out.println("Voulez-vous continuer :");
			System.out.println("(Oui : 1; Non : 2) ?");
			continuer = sc.nextInt();
		}
	}

}