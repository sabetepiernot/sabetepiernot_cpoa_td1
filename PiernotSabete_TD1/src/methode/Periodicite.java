package methode;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Periodicite extends Connexion {

	public void add(String lib) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("INSERT INTO Periodicite (libelle) VALUES ('" + lib + "');");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void edit(int id, String lib) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate(
					"UPDATE Periodicite SET libelle = '" + lib + "' WHERE id_periodicite = '" + id + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void delete(int id) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("DELETE FROM Periodicite WHERE id_periodicite = '" + id + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void print() {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			ResultSet res = requete
					.executeQuery("SELECT id_periodicite, libelle FROM Periodicite ORDER BY id_periodicite;");

			while (res.next()) {
				int no = res.getInt(1);
				String nom = res.getString("libelle");
				System.out.println(no + " " + nom);
			}

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}
}

// CTRL + SHIFT + O -> import
// CTRL + SHIFT + F -> Formate le code