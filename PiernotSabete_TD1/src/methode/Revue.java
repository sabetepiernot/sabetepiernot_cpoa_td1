package methode;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Revue extends Connexion {
	public void add(String titre, String desc, float tarif, String visuel, int id_per) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("INSERT INTO Revue (titre, description, tarif_numero, visuel, id_periodicite) VALUES ('" + titre + "','" + desc + "','" + tarif + "','" + visuel + "','" + id_per + "');");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void edit(int id, String titre, String desc, float tarif, String visuel, int id_per) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate(
					"UPDATE Revue SET titre = '" + titre + "', description = '" + desc + "', tarif_numero = '" + tarif + "', visuel = '" + visuel + "', id_periodicite = '" + id_per + "' WHERE id_revue = '" + id + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void delete(int id) {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("DELETE FROM Revue WHERE id_revue = '" + id + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}

	public void print() {
		try {
			Connection laConnexion = creeConnexion();
			Statement requete = laConnexion.createStatement();
			ResultSet res = requete
					.executeQuery("SELECT id_revue, titre, description, tarif_numero, visuel, id_periodicite FROM Revue ORDER BY id_revue;");

			while (res.next()) {
				int no = res.getInt(1);
				String titre = res.getString("titre");
				String description = res.getString("description");
				float tarif_numero = res.getFloat("tarif_numero");
				String visuel = res.getString("visuel");
				int id_periodicite = res.getInt("id_periodicite");
				System.out.println(no + " " + titre + " " + description + " " + tarif_numero + " " + visuel + " " + id_periodicite);
			}

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
	}
}