package dao.listememoire;

import java.util.ArrayList;
import java.util.List;
import java.sql.Date;
import interfaces.IAbonnementDAO;
import metier.Abonnement;
import metier.Periodicite;

public class ListeMemoireAbonnementDAO implements IAbonnementDAO {

	private static ListeMemoireAbonnementDAO instance;

	private List<Abonnement> donnees;

	private ListeMemoireAbonnementDAO() {
		this.donnees = new ArrayList<Abonnement>();

		 this.donnees.add(new Abonnement(1, 1, new Date(2017,01,01) , new Date(2017,12,31)));
		 this.donnees.add(new Abonnement(1, 2, new Date(2017,10,01) , new Date(2017,12,31)));
		 this.donnees.add(new Abonnement(2, 3, new Date(2017,01,01) , new Date(2017,12,31)));
		 this.donnees.add(new Abonnement(2, 4, new Date(2017,10,01) , new Date(2017,12,31)));
	}

	public static ListeMemoireAbonnementDAO getInstance() {
		if (instance == null) {
			instance = new ListeMemoireAbonnementDAO();
		}
		return instance;
	}

	@Override
	public boolean create(Abonnement abo) {
		// TODO Auto-generated method stub
		try {
			for (int i = 0; i < this.donnees.size(); i++) {
				if ((abo.getIdclient() == this.donnees.get(i).getIdclient()
						&& (abo.getIdrevue() == this.donnees.get(i).getIdrevue()))) {
					return false;
				}
			}
			this.donnees.add(abo);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Abonnement abo) {
		// TODO Auto-generated method stub
		try {
			for (int i = 0; i < this.donnees.size(); i++) {
				if ((abo.getIdclient() == this.donnees.get(i).getIdclient())
						&& (abo.getIdrevue() == this.donnees.get(i).getIdrevue())) {
					this.donnees.remove(i);
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean update(Abonnement abo) {
		// TODO Auto-generated method stub
		try {
			for (int i = 0; i < this.donnees.size(); i++) {
				if ((abo.getIdclient() == this.donnees.get(i).getIdclient())
						&& (abo.getIdrevue() == this.donnees.get(i).getIdrevue())) {
					this.donnees.get(i).setDatedebut(abo.getDatedebut());
					this.donnees.get(i).setDatefin(abo.getDatefin());
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean show() {
		// TODO Auto-generated method stub
		try {
			for (int i = 0; i < this.donnees.size(); i++) {
				int idCl = this.donnees.get(i).getIdrevue();
				int idRevue = this.donnees.get(i).getIdrevue();
				Date datedeb = this.donnees.get(i).getDatedebut();
				Date datefin = this.donnees.get(i).getDatefin();

				System.out.println(idCl + " " + idRevue + " " + datedeb + " " + datefin);
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public Abonnement getById(int idcl, int idrevue) {
		// TODO Auto-generated method stub
		try {
			Abonnement a = new Abonnement(idcl, idrevue, null, null);
			for (int i = 0; i < this.donnees.size(); i++) {
				if ((a.idClientEquals(this.donnees.get(i))) && (a.idRevueEquals(this.donnees.get(i)))) {
					return this.donnees.get(i);
				}
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	@Override
	public Abonnement getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Abonnement> getAbonnementByIdClient(int idClient) {
		// TODO Auto-generated method stub
		List<Abonnement> res = new ArrayList<Abonnement>();
		try {
			Abonnement a = new Abonnement(idClient, 0, null, null);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (a.idClientEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Abonnement> getAbonnementByIdRevue(int idRevue) {
		// TODO Auto-generated method stub
		List<Abonnement> res = new ArrayList<Abonnement>();
		try {
			Abonnement a = new Abonnement(0, idRevue, null, null);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (a.idRevueEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

	public List<Abonnement> getAbonnementByDateDebut(Date dateDebut) {
		// TODO Auto-generated method stub
		List<Abonnement> res = new ArrayList<Abonnement>();
		try {
			Abonnement a = new Abonnement(0, 0, dateDebut, null);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (a.dateDebutEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

	public List<Abonnement> getAbonnementByDateFin(Date dateFin) {
		// TODO Auto-generated method stub
		List<Abonnement> res = new ArrayList<Abonnement>();
		try {
			Abonnement a = new Abonnement(0, 0, null, dateFin);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (a.dateFinEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}
}
