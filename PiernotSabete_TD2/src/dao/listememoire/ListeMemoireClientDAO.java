package dao.listememoire;

import java.util.ArrayList;
import java.util.List;
import java.sql.Date;

import interfaces.IAbonnementDAO;
import interfaces.IClientDAO;
import metier.Abonnement;
import metier.Client;
import metier.Periodicite;

public class ListeMemoireClientDAO implements IClientDAO {

	private static ListeMemoireClientDAO instance;

	private List<Client> donnees;

	private ListeMemoireClientDAO() {
		this.donnees = new ArrayList<Client>();

		 this.donnees.add(new Client(1, "Sabete", "Navid", "12", "rue de la source", "57000", "Metz", "France"));
		 this.donnees.add(new Client(2, "Piernot", "Alexis", "1", "rue de belchamps", "57000", "Metz", "France"));
	}

	public static ListeMemoireClientDAO getInstance() {
		if (instance == null) {
			instance = new ListeMemoireClientDAO();
		}
		return instance;
	}

	@Override
	public boolean create(Client cl) {
		// TODO Auto-generated method stub
		try {
			int max = 1;
			for (int i = 0; i < this.donnees.size(); i++) {
				if (this.donnees.get(i).getIdcl() >= max) {
					max = this.donnees.get(i).getIdcl() + 1;
				}
			}
			cl.setIdcl(max);
			this.donnees.add(cl);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Client cl) {
		// TODO Auto-generated method stub
		try {
			for (int i = 0; i < this.donnees.size(); i++) {
				if ((cl.getIdcl() == this.donnees.get(i).getIdcl())) {
					this.donnees.remove(i);
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean update(Client cl) {
		// TODO Auto-generated method stub
		try {
			for (int i = 0; i < this.donnees.size(); i++) {
				if ((cl.getIdcl() == this.donnees.get(i).getIdcl())) {
					this.donnees.get(i).setNom(cl.getNom());
					this.donnees.get(i).setPrenom(cl.getPrenom());
					this.donnees.get(i).setNorue(cl.getNorue());
					this.donnees.get(i).setVoie(cl.getVoie());
					this.donnees.get(i).setCodepostal(cl.getCodepostal());
					this.donnees.get(i).setVille(cl.getVille());
					this.donnees.get(i).setPays(cl.getPays());
				}
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean show() {
		// TODO Auto-generated method stub
		try {
			for (int i = 0; i < this.donnees.size(); i++) {
				int idCl = this.donnees.get(i).getIdcl();
				String nom = this.donnees.get(i).getNom();
				String prenom = this.donnees.get(i).getPrenom();
				String norue = this.donnees.get(i).getNorue();
				String voie = this.donnees.get(i).getVoie();
				String cp = this.donnees.get(i).getCodepostal();
				String ville = this.donnees.get(i).getVille();
				String pays = this.donnees.get(i).getPays();

				System.out.println(idCl + " " + nom + " " + prenom + " " + norue + " " + voie + " " + cp + " " + ville
						+ " " + pays);
			}
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public Client getById(int id) {
		// TODO Auto-generated method stub
		try {
			Client c = new Client(id, null, null, null, null, null, null, null);
			for (int i=0;i<this.donnees.size();i++) {
				if (c.idClientEquals(this.donnees.get(i))) {
					return this.donnees.get(i);
				}
			}
		}
		catch (Exception e) {
			return null;
		}
		return null;
	}

	@Override
	public List<Client> getClientByNom(String nom) {
		// TODO Auto-generated method stub
		List<Client> res = new ArrayList<Client>();
		try {
			Client c = new Client(0, nom, null, null, null, null, null, null);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (c.nomEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Client> getClientByPrenom(String prenom) {
		// TODO Auto-generated method stub
		List<Client> res = new ArrayList<Client>();
		try {
			Client c = new Client(0, null, prenom, null, null, null, null, null);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (c.prenomEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Client> getClientByNoRue(String noRue) {
		// TODO Auto-generated method stub
		List<Client> res = new ArrayList<Client>();
		try {
			Client c = new Client(0, null, null, noRue, null, null, null, null);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (c.noRueEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Client> getClientByVoie(String voie) {
		// TODO Auto-generated method stub
		List<Client> res = new ArrayList<Client>();
		try {
			Client c = new Client(0, null, null, null, voie, null, null, null);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (c.voieEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Client> getClientByCodePostal(String cp) {
		// TODO Auto-generated method stub
		List<Client> res = new ArrayList<Client>();
		try {
			Client c = new Client(0, null, null, null, null, cp, null, null);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (c.codePostalEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Client> getClientByVille(String ville) {
		// TODO Auto-generated method stub
		List<Client> res = new ArrayList<Client>();
		try {
			Client c = new Client(0, null, null, null, null, null, ville, null);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (c.villeEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Client> getClientByPays(String pays) {
		// TODO Auto-generated method stub
		List<Client> res = new ArrayList<Client>();
		try {
			Client c = new Client(0, null, null, null, null, null, null, pays);
			for (int i = 0; i < this.donnees.size(); i++) {
				if (c.paysEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

}
