package dao.listememoire;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import interfaces.IPeriodiciteDAO;
import metier.Periodicite;
import metier.Revue;

public class ListeMemoirePeriodiciteDAO implements IPeriodiciteDAO {

	private static ListeMemoirePeriodiciteDAO instance;
	private List<Periodicite> donnees;

	public static ListeMemoirePeriodiciteDAO getInstance() {

		if (instance == null) {
			instance = new ListeMemoirePeriodiciteDAO();
		}
		return instance;
	}

	private ListeMemoirePeriodiciteDAO() {
		this.donnees = new ArrayList<Periodicite>();

		this.donnees.add(new Periodicite(1, "Mensuel"));
		this.donnees.add(new Periodicite(2, "Quotidien"));
	}

	@Override
	public Periodicite getById(int id) {
		try {
			Periodicite p = new Periodicite(id, null);
			for (int i=0;i<this.donnees.size();i++) {
				if (p.idPeriodiciteEquals(this.donnees.get(i))) {
					return this.donnees.get(i);
				}
			}
		}
		catch (Exception e) {
			return null;
		}
		return null;
	}

	@Override
	public boolean create(Periodicite p) {
		try {
			int max = 1;
			for (int i = 0; i < this.donnees.size(); i++) {
				if (this.donnees.get(i).getIdPeriodicite() >= max) {
					max = this.donnees.get(i).getIdPeriodicite() + 1;
				}
			}
			p.setIdPeriodicite(max);
			this.donnees.add(p);
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Periodicite p) {
		try {
			for (int i=0;i<this.donnees.size();i++) {
				if (p.getIdPeriodicite() == this.donnees.get(i).getIdPeriodicite()) {
					this.donnees.remove(i);
				}
			}
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean update(Periodicite p) {
		try {
			for (int i=0;i<this.donnees.size();i++) {
				if (p.getIdPeriodicite() == this.donnees.get(i).getIdPeriodicite()) {
					this.donnees.get(i).setIdPeriodicite(p.getIdPeriodicite());
					this.donnees.get(i).setLibelle(p.getLibelle());
				}
			}
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean show() {
		try {
			for (int i=0;i<this.donnees.size();i++) {
				int idPeriodicite = this.donnees.get(i).getIdPeriodicite();
				String libelle= this.donnees.get(i).getLibelle();
				
				System.out.println(idPeriodicite + " " + libelle);
			}
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public List<Periodicite> getPeriodiciteByLibelle(String libelle) {
		List<Periodicite> res = new ArrayList<Periodicite>();
		Periodicite p = new Periodicite(0, libelle);
		try {
			for (int i=0;i<this.donnees.size();i++) {
				if (p.libelleEquals(this.donnees.get(i))) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

}

