package dao.mysql;

import methode.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import interfaces.IAbonnementDAO;
import metier.Abonnement;
import metier.Client;

public class MySQLAbonnementDAO implements IAbonnementDAO {

	private static MySQLAbonnementDAO instance;

	private MySQLAbonnementDAO() {

	}

	public static MySQLAbonnementDAO getInstance() {
		if (instance == null) {
			instance = new MySQLAbonnementDAO();

		}
		return instance;
	}

	public Abonnement getById(int idcl, int idrevue) {
		Abonnement abo = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			PreparedStatement requete = laConnexion.prepareStatement("select * from Abonnement where id_client =? and id_revue =?;");
			requete.setInt(1, idcl);
			requete.setInt(2, idrevue);
			ResultSet res = requete.executeQuery();
			while (res.next()) {
				abo.setIdclient(res.getInt(1));
				abo.setIdrevue(res.getInt(2));
				abo.setDatedebut(res.getDate("date_debut"));
				abo.setDatefin(res.getDate("date_fin"));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return abo;
	}

	@Override
	public boolean create(Abonnement abo) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete
					.executeUpdate("INSERT INTO Abonnement (id_client, id_revue, date_debut, date_fin) VALUES ('"
							+ abo.getIdclient() + "','" + abo.getIdrevue() + "',str_to_date('" + abo.getDatedebut()
							+ "', '%d/%m/%Y'),str_to_date('" + abo.getDatefin() + "', '%d/%m/%Y'));");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean update(Abonnement abo) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("UPDATE Abonnement SET date_debut = str_to_date('" + abo.getDatedebut()
					+ "','%d/%m/%Y'), date_fin = str_to_date('" + abo.getDatefin() + "','%d/%m/%Y') WHERE id_client = '"
					+ abo.getIdclient() + "' AND id_revue = '" + abo.getIdrevue() + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Abonnement abo) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("DELETE FROM Abonnement WHERE id_client = '" + abo.getIdclient()
					+ "' AND id_revue = '" + abo.getIdrevue() + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean show() {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet res = requete.executeQuery("SELECT * FROM Abonnement ORDER BY id_client, id_revue;");

			while (res.next()) {
				int no = res.getInt(1);
				int no2 = res.getInt(2);
				String date_debut = res.getString("date_debut");
				String date_fin = res.getString("date_fin");
				System.out.println(no + " " + no2 + " " + date_debut + " " + date_fin);
			}

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public Abonnement getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Abonnement> getAbonnementByIdClient(int idClient) {
		// TODO Auto-generated method stub
		List<Abonnement> resabo = new ArrayList<Abonnement>();
		Abonnement abo = null;
		
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Abonnement where id_client = '" + idClient+ "' ORDER BY id_client, id_revue ;");

			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				int idrevue=(resReq.getInt(2));
				Date datedeb=(resReq.getDate("date_debut"));
				Date datefin=(resReq.getDate("date_fin"));
				resabo.add(new Abonnement(idcl, idrevue, datedeb, datefin));

				
			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return resabo;
	}

	@Override
	public List<Abonnement> getAbonnementByIdRevue(int idRevue) {
		// TODO Auto-generated method stub
		List<Abonnement> resabo = new ArrayList<Abonnement>();
		Abonnement abo = null;
		
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Abonnement where id_revue = '" + idRevue+ "' ORDER BY id_client, id_revue;");

			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				int idrevue=(resReq.getInt(2));
				Date datedeb=(resReq.getDate("date_debut"));
				Date datefin=(resReq.getDate("date_fin"));
				resabo.add(new Abonnement(idcl, idrevue, datedeb, datefin));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return resabo;
	}

	public List<Abonnement> getAbonnementByDateDebut(Date dateDebut) {
		// TODO Auto-generated method stub
		List<Abonnement> resabo = new ArrayList<Abonnement>();
		Abonnement abo = null;
		
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Abonnement where date_debut = '" + dateDebut + "' ORDER BY id_client, id_revue;");

			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				int idrevue=(resReq.getInt(2));
				Date datedeb=(resReq.getDate("date_debut"));
				Date datefin=(resReq.getDate("date_fin"));
				resabo.add(new Abonnement(idcl, idrevue, datedeb, datefin));


			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return resabo;
	}


	public List<Abonnement> getAbonnementByDateFin(Date dateFin) {
		// TODO Auto-generated method stub
		List<Abonnement> resabo = new ArrayList<Abonnement>();
		Abonnement abo = null;
		
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Abonnement where date_fin = '" + dateFin + "' ORDER BY id_client, id_revue;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				int idrevue=(resReq.getInt(2));
				Date datedeb=(resReq.getDate("date_debut"));
				Date datefin=(resReq.getDate("date_fin"));
				resabo.add(new Abonnement(idcl, idrevue, datedeb, datefin));


			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return resabo;
	}

	
}
