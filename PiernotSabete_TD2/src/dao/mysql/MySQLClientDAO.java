package dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import interfaces.IClientDAO;
import methode.Connexion;
import metier.Client;

public class MySQLClientDAO implements IClientDAO {

	private static MySQLClientDAO instance;

	private MySQLClientDAO() {

	}

	public static MySQLClientDAO getInstance() {
		if (instance == null) {
			instance = new MySQLClientDAO();

		}
		return instance;
	}

	public Client getById(int id) {
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			PreparedStatement requete = laConnexion.prepareStatement("select * from Client where id_client =?;");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			while (res.next()) {
				cl.setIdcl(res.getInt(1));
				cl.setNom(res.getString("nom"));
				cl.setPrenom(res.getString("prenom"));
				cl.setNorue(res.getString("no_rue"));
				cl.setVoie(res.getString("voie"));
				cl.setCodepostal(res.getString("code_postal"));
				cl.setVille(res.getString("ville"));
				cl.setPays(res.getString("pays"));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return cl;
	}

	@Override
	public boolean create(Client cl) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete
					.executeUpdate("INSERT INTO Client (nom, prenom, no_rue, voie, code_postal, ville, pays) VALUES ('"
							+ cl.getNom() + "','" + cl.getPrenom() + "','" + cl.getNorue() + "','" + cl.getVoie()
							+ "','" + cl.getCodepostal() + "','" + cl.getVille() + "','" + cl.getPays() + "');");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Client cl) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("DELETE FROM Client WHERE id_client = '" + cl.getIdcl() + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}

		return true;
	}

	@Override
	public boolean update(Client cl) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("UPDATE Client SET nom = '" + cl.getNom() + "', prenom = '" + cl.getPrenom()
					+ "', no_rue = '" + cl.getNorue() + "', voie = '" + cl.getVoie() + "', code_postal = '"
					+ cl.getCodepostal() + "', ville = '" + cl.getVille() + "', pays = '" + cl.getPays()
					+ "' WHERE id_client = '" + cl.getIdcl() + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean show() {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet res = requete.executeQuery(
					"SELECT id_client, nom, prenom, no_rue, voie, code_postal, ville, pays FROM Client ORDER BY id_client;");

			while (res.next()) {
				int no = res.getInt(1);
				String nom = res.getString("nom");
				String prenom = res.getString("prenom");
				String no_rue = res.getString("no_rue");
				String voie = res.getString("voie");
				String code_postal = res.getString("code_postal");
				String ville = res.getString("ville");
				String pays = res.getString("pays");
				System.out.println(no + " " + nom + " " + prenom + " " + no_rue + " " + voie + " " + code_postal + " "
						+ ville + " " + pays);
			}

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;

	}

	@Override
	public List<Client> getClientByNom(String nom) {
		// TODO Auto-generated method stub
		
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where nom = '" + nom + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, voie, cp, ville, pays));

				
			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByPrenom(String prenom) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where prenom = '" + prenom + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String Prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, Prenom, norue, voie, cp, ville, pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByNoRue(String noRue) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where no_rue = '" + noRue + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, voie, cp, ville, pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByVoie(String voie) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where voie = '" + voie + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String Voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, Voie, cp, ville, pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByCodePostal(String cp) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where code_postal = '" + cp + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String codepostal=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, voie, codepostal, ville, pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByVille(String ville) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where ville = '" + ville + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String Ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, voie, cp, Ville, pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByPays(String pays) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where pays = '" + pays + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String Pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, voie, cp, ville, Pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}
}
