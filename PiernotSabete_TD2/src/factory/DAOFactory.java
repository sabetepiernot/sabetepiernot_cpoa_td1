package factory;

import enumerations.EPersistance;
import interfaces.IAbonnementDAO;
import interfaces.IClientDAO;
import interfaces.IPeriodiciteDAO;
import interfaces.IRevueDAO;

public abstract class DAOFactory {
	public static DAOFactory getDAOFactory(EPersistance cible) {
		DAOFactory daoF = null;

		switch (cible) {
		case MYSQL:
			daoF = new MySQLDAOFactory();
			break;
		case MEMORY:
			daoF = new ListeMemoireDAOFactory();
			break;
		}
		return daoF;
	}

	public abstract IRevueDAO getRevueDAO();

	public abstract IClientDAO getClientDAO();
	
	public abstract IAbonnementDAO getAbonnementDAO();

	public abstract IPeriodiciteDAO getPeriodiciteDAO();

}
