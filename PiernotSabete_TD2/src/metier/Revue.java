package metier;

public class Revue {
	private int idRevue;
	private String titre;
	private String description;
	private float tarifNumero;
	private String visuel;
	private int idPeriodicite;

	public Revue(int idRevue, String titre, String description, float tarifNumero, String visuel, int idPeriodicite) {
		this.idRevue = idRevue;
		this.titre = titre;
		this.description = description;
		this.tarifNumero = tarifNumero;
		this.visuel = visuel;
		this.idPeriodicite = idPeriodicite;
	}

	public int getIdRevue() {
		return idRevue;
	}

	public void setIdRevue(int idRevue) {
		this.idRevue = idRevue;
	}

	public String getTitre() {
		return titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getTarifNumero() {
		return tarifNumero;
	}

	public void setTarifNumero(float tarifNumero) {
		this.tarifNumero = tarifNumero;
	}

	public String getVisuel() {
		return visuel;
	}

	public void setVisuel(String visuel) {
		this.visuel = visuel;
	}

	public int getIdPeriodicite() {
		return idPeriodicite;
	}

	public void setIdPeriodicite(int idPeriodicite) {
		this.idPeriodicite = idPeriodicite;
	}
	
	public boolean idRevueEquals(Revue r) {
		return (this.idRevue == r.idRevue);
	}
	
	public boolean titreEquals(Revue r) {
		return (this.titre.equals(r.titre));
	}
	
	public boolean descriptionEquals(Revue r) {
		return (this.description.equals(r.description));
	}
	
	public boolean tarifNumeroEquals(Revue r) {
		return (this.tarifNumero == r.tarifNumero);
	}
	
	public boolean visuelEquals(Revue r) {
		return (this.visuel.equals(r.visuel));
	}
	
	public boolean idPeriodiciteEquals(Revue r) {
		return (this.idPeriodicite == r.idPeriodicite);
	}

}