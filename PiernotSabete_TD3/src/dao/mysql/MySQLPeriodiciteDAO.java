package dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import interfaces.IDAO;
import interfaces.IPeriodiciteDAO;
import interfaces.IRevueDAO;
import methode.Connexion;
import metier.Periodicite;
import metier.Revue;

public class MySQLPeriodiciteDAO implements  IPeriodiciteDAO {
	private static MySQLPeriodiciteDAO instance;

	private MySQLPeriodiciteDAO() {
		
	}
	
	public static MySQLPeriodiciteDAO getInstance() {
		if (instance == null) {
			instance = new MySQLPeriodiciteDAO();
		}
		return instance;
	}

	public Periodicite getById(int id) {
		Periodicite p = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM periodicite WHERE no_periodicite=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			while (res.next()) {
				p.setIdPeriodicite(res.getInt(1));
				p.setLibelle(res.getString("libelle"));
			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
		return p;
	}

	@Override
	public boolean create(Periodicite p) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("INSERT INTO Periodicite (libelle) VALUES ('" + p.getLibelle() + "');");
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Periodicite p) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("DELETE FROM Periodicite WHERE id_periodicite = '" + p.getIdPeriodicite() + "';");
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean update(Periodicite p) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate(
					"UPDATE Periodicite SET libelle = '" + p.getLibelle() + "' WHERE id_periodicite = '" + p.getIdPeriodicite() + "';");
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean show() {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet res = requete.executeQuery("SELECT id_periodicite, libelle FROM Periodicite ORDER BY id_periodicite;");
			while (res.next()) {
				int no = res.getInt(1);
				String nom = res.getString("libelle");
				System.out.println(no + " " + nom);
			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public List<Periodicite> getPeriodiciteByLibelle(String libelle) {
		List<Periodicite> res = new ArrayList<Periodicite>();
		try {
			Periodicite p = new Periodicite(0, libelle);
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("SELECT * FROM Periodicite WHERE libelle='" + libelle + "' ORDER BY id_periodicite;");
			while (resReq.next()) {
				if (p.libelleEquals(new Periodicite(0, resReq.getString(2)))) {
					int id = resReq.getInt(1);
					res.add(new Periodicite(id, libelle));
				}
			}
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

	
}
