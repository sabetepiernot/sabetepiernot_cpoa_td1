package dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import interfaces.IRevueDAO;
import methode.Connexion;
import metier.Periodicite;
import metier.Revue;

public class MySQLRevueDAO implements IRevueDAO {
	
	private static MySQLRevueDAO instance;

	private MySQLRevueDAO() {

	}

	public static MySQLRevueDAO getInstance() {
		if (instance == null) {
			instance = new MySQLRevueDAO();
		}
		return instance;
	}

	public Revue getById(int id) {
		Revue r = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			PreparedStatement requete = laConnexion.prepareStatement("SELECT * FROM revue WHERE no_revue=?");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();

			while (res.next()) {
				r.setIdRevue(res.getInt(1));
				r.setTitre(res.getString("titre"));
				r.setDescription(res.getString("description"));
				r.setTarifNumero(res.getFloat("tarif_numero"));
				r.setVisuel(res.getString("visuel"));
				r.setIdRevue(res.getInt(6));
			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}
		return r;
	}

	@Override
	public boolean create(Revue r) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate(
					"INSERT INTO Revue (titre, description, tarif_numero, visuel, id_periodicite) VALUES ('"
							+ r.getTitre() + "','" + r.getDescription() + "','" + r.getTarifNumero() + "','"
							+ r.getVisuel() + "','" + r.getIdPeriodicite() + "');");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Revue r) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("DELETE FROM Revue WHERE id_revue = '" + r.getIdRevue() + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean update(Revue r) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("UPDATE Revue SET titre = '" + r.getTitre() + "', description = '"
					+ r.getDescription() + "', tarif_numero = '" + r.getTarifNumero() + "', visuel = '" + r.getVisuel()
					+ "', id_periodicite = '" + r.getIdPeriodicite() + "' WHERE id_revue = '" + r.getIdRevue() + "';");
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean show() {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet res = requete.executeQuery("SELECT * FROM Revue ORDER BY id_revue;");

			while (res.next()) {
				int no = res.getInt(1);
				String titre = res.getString("titre");
				String description = res.getString("description");
				float tarif_numero = res.getFloat("tarif_numero");
				String visuel = res.getString("visuel");
				int id_periodicite = res.getInt("id_periodicite");
				System.out.println(no + " " + titre + " " + description + " " + tarif_numero + " " + visuel + " "
						+ id_periodicite);
			}

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public List<Revue> getByTitre(String titre) {
		List<Revue> res = new ArrayList<Revue>();
		try {
			Revue r = new Revue(0, titre, null, (float) 0, null, 0);
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("SELECT * FROM Revue WHERE titre='" + titre + "' ORDER BY id_revue;");
			while (resReq.next()) {
				if (r.titreEquals(new Revue(0, resReq.getString(2), null, (float) 0, null, 0)) ) {
					int idRevue = resReq.getInt(1);
					String description = resReq.getString("description");
					float tarif_numero = resReq.getFloat("tarif_numero");
					String visuel = resReq.getString("visuel");
					int id_periodicite = resReq.getInt("id_periodicite");
					res.add(new Revue(idRevue, titre, description, tarif_numero, visuel, id_periodicite));
				}
			}
			
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Revue> getByDescription(String description) {
		List<Revue> res = new ArrayList<Revue>();
		try {
			Revue r = new Revue(0, null, description, (float) 0, null, 0);
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("SELECT * FROM Revue WHERE description='" + description + "' ORDER BY id_revue;");
			while (resReq.next()) {
				if (r.descriptionEquals(new Revue(0, null, resReq.getString(3), (float) 0, null, 0))) {
					int idRevue = resReq.getInt(1);
					String titre = resReq.getString("titre");
					float tarif_numero = resReq.getFloat("tarif_numero");
					String visuel = resReq.getString("visuel");
					int id_periodicite = resReq.getInt("id_periodicite");
					res.add(new Revue(idRevue, titre, description, tarif_numero, visuel, id_periodicite));
				}
			}
			
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Revue> getByTarifNumero(float tarif) {
		List<Revue> res = new ArrayList<Revue>();
		try {
			Revue r = new Revue(0, null, null, tarif, null, 0);
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("SELECT * FROM Revue WHERE tarif='" + tarif + "' ORDER BY id_revue;");
			while (resReq.next()) {
				if (r.tarifNumeroEquals(new Revue(0, null, null, resReq.getFloat(4), null, 0))) {
					int idRevue = resReq.getInt(1);
					String titre = resReq.getString("titre");
					String description = resReq.getString("description");
					String visuel = resReq.getString("visuel");
					int id_periodicite = resReq.getInt("id_periodicite");
					res.add(new Revue(idRevue, titre, description, tarif, visuel, id_periodicite));
				}
			}
			
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Revue> getByVisuel(String visuel) {
		List<Revue> res = new ArrayList<Revue>();
		try {
			Revue r = new Revue(0, null, null, (float) 0, visuel, 0);
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("SELECT * FROM Revue WHERE visuel='" + visuel + "' ORDER BY id_revue;");
			while (resReq.next()) {
				if (r.visuelEquals(new Revue(0, null, null, (float) 0, resReq.getString(4), 0))) {
					int idRevue = resReq.getInt(1);
					String titre = resReq.getString("titre");
					String description = resReq.getString("description");
					float tarif_numero = resReq.getFloat("tarif_numero");
					int id_periodicite = resReq.getInt("id_periodicite");
					res.add(new Revue(idRevue, titre, description, tarif_numero, visuel, id_periodicite));
				}
			}
			
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Revue> getByIdPeriodicite(int idPeriodicite) {
		List<Revue> res = new ArrayList<Revue>();
		try {
			Revue r = new Revue(0, null, null, (float) 0, null, idPeriodicite);
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("SELECT * FROM Revue WHERE idPeriodicite='" + idPeriodicite + "' ORDER BY id_revue;");
			while (resReq.next()) {
				if (r.idPeriodiciteEquals(new Revue(0, null, null, (float) 0, null, resReq.getInt(5)))) {
					int idRevue = resReq.getInt(1);
					String titre = resReq.getString("titre");
					String description = resReq.getString("description");
					float tarif_numero = resReq.getFloat("tarif_numero");
					String visuel = resReq.getString("visuel");
					res.add(new Revue(idRevue, titre, description, tarif_numero, visuel, idPeriodicite));
				}
			}
			
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

}
