package factory;

import dao.listememoire.ListeMemoirePeriodiciteDAO;
import dao.listememoire.ListeMemoireRevueDAO;
import dao.listememoire.ListeMemoireClientDAO;
import dao.listememoire.ListeMemoireAbonnementDAO;
import interfaces.IAbonnementDAO;
import interfaces.IClientDAO;
import interfaces.IPeriodiciteDAO;
import interfaces.IRevueDAO;

public class ListeMemoireDAOFactory extends DAOFactory {

	@Override
	public IRevueDAO getRevueDAO() {
		return ListeMemoireRevueDAO.getInstance();
	}

	@Override
	public IClientDAO getClientDAO() {
		return ListeMemoireClientDAO.getInstance();
	}
	
	@Override
	public IPeriodiciteDAO getPeriodiciteDAO() {
		return ListeMemoirePeriodiciteDAO.getInstance();
	}
	
	@Override
	public IAbonnementDAO getAbonnementDAO() {
		return ListeMemoireAbonnementDAO.getInstance();
	}

}
