package factory;

import dao.mysql.MySQLAbonnementDAO;
import dao.mysql.MySQLClientDAO;
import dao.mysql.MySQLPeriodiciteDAO;
import dao.mysql.MySQLRevueDAO;
import interfaces.IAbonnementDAO;
import interfaces.IClientDAO;
import interfaces.IPeriodiciteDAO;
import interfaces.IRevueDAO;

public class MySQLDAOFactory extends DAOFactory {

	@Override
	public IRevueDAO getRevueDAO() {
		return MySQLRevueDAO.getInstance();
	}

	@Override
	public IClientDAO getClientDAO() {
		return MySQLClientDAO.getInstance();
	}
	
	@Override
	public IPeriodiciteDAO getPeriodiciteDAO() {
		return MySQLPeriodiciteDAO.getInstance();
	}
	
	@Override
	public IAbonnementDAO getAbonnementDAO() {
		return MySQLAbonnementDAO.getInstance();
	}

}
