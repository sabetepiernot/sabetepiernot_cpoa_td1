package interfaces;

import java.util.List;

import metier.Revue;

public interface IRevueDAO extends IDAO<Revue> {

	List<Revue> getByTitre(String titre);
	List<Revue> getByDescription(String description);
	List<Revue> getByTarifNumero(float tarif);
	List<Revue> getByVisuel(String visuel);
	List<Revue> getByIdPeriodicite(int idPeriodicite);
}
