package methode;

import java.sql.*;

public class Connexion {
	private static Connection instance;
	private String url = "jdbc:mysql://localhost:8000/piernot3u_dbcpoavance";
	//private String url = "jdbc:mysql://infodb.iutmetz.univ-lorraine.fr:3306/piernot3u_dbcpoavance";
	private String login = "piernot3u_appli";
	private String pwd = "31612718";
	
	private Connexion() {
		try {
			instance = DriverManager.getConnection(url, login, pwd);
		} catch (SQLException sqle) {
			System.out.println("Erreur connexion" + sqle.getMessage());
		}
	}

	public static Connection getInstance() {
		if (instance == null) {
			new Connexion();
		}
		return instance;
	}

}