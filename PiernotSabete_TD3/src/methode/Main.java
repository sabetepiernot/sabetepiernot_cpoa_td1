package methode;

import metier.*;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import enumerations.EPersistance;
import factory.DAOFactory;

public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		String videur = "";
		int persistance = 0;
		int continuer = 1;
		int table = 0;
		int action = 0;
		int action_continuer = 1;
		int affiche = 1;
		
		int id_client = 0;
		int id_revue = 0;
		String date_debut = "";
		String date_fin = "";
		String nom = "";
		String prenom = "";
		String no_rue = "";
		String voie = "";
		String code_postal = "";
		String ville = "";
		String pays = "";
		int id_periodicite = 0;
		String libelle = "";
		String titre = "";
		String description = "";
		String visuel = "";
		double tarif_numero = 0;
		int j = 0;
		int m = 0;
		int a = 0;
		
		DAOFactory daos = null;
		
		do {
			System.out.println("Voulez-vous travailler sur :");
			System.out.println("(MySQL : 1; Une liste memoire : 2) ?");
			persistance = sc.nextInt();
		} while (persistance != 1 && persistance != 2);
		if (persistance == 1) {
			daos = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		}
		else if (persistance == 2) {
			daos = DAOFactory.getDAOFactory(EPersistance.MEMORY);
		}
		
		while (continuer == 1) {
			do {
			System.out.println("Sur quel tableau voulez-vous effectuer une action :");
			System.out.println("(Abonnement : 1; Client : 2; Periodicite : 3; Revue : 4) ?");
			table = sc.nextInt();
			} while (table != 1 && table != 2 && table != 3 && table != 4);
			if (table == 1) {
				do {
					do {
						System.out.println("Quelle action voulez-vous realiser :");
						System.out.println("(Ajouter une ligne : 1; Modifier une ligne : 2; Supprimer une ligne : 3; Afficher : 4) ?");
						action = sc.nextInt();
					} while (action != 1 && action != 2 && action != 3 && action != 4);
					if (action == 1) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_client : ");
						id_client = sc.nextInt();
						System.out.println("id_revue : ");
						id_revue = sc.nextInt();
						videur = sc.nextLine();
						System.out.println("date_debut (jj/mm/aaaa) : ");
						System.out.println("jour en chiffre : ");
						j = sc.nextInt();
						System.out.println("mois en chiffre : ");
						m = sc.nextInt();
						System.out.println("annee en chiffre : ");
						a = sc.nextInt();
						Date dd = new Date(j, m, a);
						System.out.println("date_fin (jj/mm/aaaa) : ");
						System.out.println("jour en chiffre : ");
						j = sc.nextInt();
						System.out.println("mois en chiffre : ");
						m = sc.nextInt();
						System.out.println("annee en chiffre : ");
						a = sc.nextInt();
						Date df = new Date(j, m, a);
						
						daos.getAbonnementDAO().create(new Abonnement(id_client, id_revue, dd, df));
					}
					else if (action == 2) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_client : ");
						id_client = sc.nextInt();
						System.out.println("id_revue : ");
						id_revue = sc.nextInt();
						videur = sc.nextLine();
						System.out.println("date_debut (jj/mm/aaaa) : ");
						System.out.println("jour en chiffre : ");
						j = sc.nextInt();
						System.out.println("mois en chiffre : ");
						m = sc.nextInt();
						System.out.println("annee en chiffre : ");
						a = sc.nextInt();
						Date dd = new Date(j, m, a);
						System.out.println("date_fin (jj/mm/aaaa) : ");
						System.out.println("jour en chiffre : ");
						j = sc.nextInt();
						System.out.println("mois en chiffre : ");
						m = sc.nextInt();
						System.out.println("annee en chiffre : ");
						a = sc.nextInt();
						Date df = new Date(j, m, a);
						
						daos.getAbonnementDAO().update(new Abonnement(id_client, id_revue, dd, df));
					}
					else if (action == 3) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_client : ");
						id_client = sc.nextInt();
						System.out.println("id_revue : ");
						id_revue = sc.nextInt();
						
						daos.getAbonnementDAO().delete(new Abonnement(id_client, id_revue, null, null));
					}
					else if (action == 4) {
						do {
							System.out.println("Que voulez-vous afficher :");
							System.out.println("(La table : 1; Par id client : 2; Par id revue : 3; Par date de debut : 4; Par date de fin : 5) ?");
							affiche = sc.nextInt();
						} while (affiche != 1 && affiche != 2 && affiche != 3 && affiche != 4 && affiche != 5);
						List<Abonnement> la = new ArrayList<Abonnement>();
						if (affiche == 1) {
							daos.getAbonnementDAO().show();
						}
						else if (affiche == 2) {
							System.out.println("id client : ");
							id_client = sc.nextInt();
							
							la = daos.getAbonnementDAO().getAbonnementByIdClient(id_client);
						}
						else if (affiche == 3) {
							System.out.println("id revue : ");
							id_revue = sc.nextInt();
							
							la = daos.getAbonnementDAO().getAbonnementByIdRevue(id_revue);
						}
						else if (affiche == 4) {
							System.out.println("date_debut (jj/mm/aaaa) : ");
							System.out.println("jour en chiffre : ");
							j = sc.nextInt();
							System.out.println("mois en chiffre : ");
							m = sc.nextInt();
							System.out.println("annee en chiffre : ");
							a = sc.nextInt();
							Date dd = new Date(j, m, a);
							
							la = daos.getAbonnementDAO().getAbonnementByDateDebut(dd);
						}
						else if (affiche == 5) {
							System.out.println("date_fin (jj/mm/aaaa) : ");
							System.out.println("jour en chiffre : ");
							j = sc.nextInt();
							System.out.println("mois en chiffre : ");
							m = sc.nextInt();
							System.out.println("annee en chiffre : ");
							a = sc.nextInt();
							Date df = new Date(j, m, a);
							
							la = daos.getAbonnementDAO().getAbonnementByDateFin(df);
						}
						if (affiche != 1) {
							for (int i=0; i<la.size(); i++) {
								System.out.println(la.get(i).getIdclient() + " " + la.get(i).getIdrevue() + " " + la.get(i).getDatedebut() + " " + la.get(i).getDatefin());
							}
						}
					}
					do {
						System.out.println("Continuer les action sur cette table :");
						System.out.println("(Oui : 1; Non : 2) :");
						action_continuer = sc.nextInt();
					} while (action_continuer != 1 && action_continuer != 2);
				} while(action_continuer == 1);
			}
			else if (table == 2) {
				do {
					do {
						System.out.println("Quelle action voulez-vous realiser :");
						System.out.println("(Ajouter une ligne : 1; Modifier une ligne : 2; Supprimer une ligne : 3; Afficher : 4; Ajouter un fichier CSV : 5) ?");
						action = sc.nextInt();
					} while (action != 1 && action != 2 && action != 3 && action != 4 && action != 5);
					videur = sc.nextLine();
					if (action == 1) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("nom : ");
						nom = sc.nextLine();
						System.out.println("prenom : ");
						prenom = sc.nextLine();
						System.out.println("no_rue : ");
						no_rue = sc.nextLine();
						System.out.println("voie : ");
						voie = sc.nextLine();
						System.out.println("code_postal : ");
						code_postal = sc.nextLine();
						System.out.println("ville : ");
						ville = sc.nextLine();
						System.out.println("pays : ");
						pays = sc.nextLine();
						
						daos.getClientDAO().create(new Client(0, nom, prenom, no_rue, voie, code_postal, ville, pays));
					}
					else if (action == 2) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_client : ");
						id_client = sc.nextInt();
						videur = sc.nextLine();
						System.out.println("nom : ");
						nom = sc.nextLine();
						System.out.println("prenom : ");
						prenom = sc.nextLine();
						System.out.println("no_rue : ");
						no_rue = sc.nextLine();
						System.out.println("voie : ");
						voie = sc.nextLine();
						System.out.println("code_postal : ");
						code_postal = sc.nextLine();
						System.out.println("ville : ");
						ville = sc.nextLine();
						System.out.println("pays : ");
						pays = sc.nextLine();
						
						daos.getClientDAO().update(new Client(id_client, nom, prenom, no_rue, voie, code_postal, ville, pays));
					}
					else if (action == 3) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_client : ");
						id_client = sc.nextInt();
						
						daos.getClientDAO().delete(new Client(id_client, null, null, null, null, null, null, null));
					}
					else if (action == 4) {
						do {
							System.out.println("Que voulez-vous afficher :");
							System.out.println("(La table : 1; Par nom : 2; Par prenom : 3; Par numero rue : 4; Par voie : 5; Par code postal : 6; Par ville : 7; Par pays : 8; Par cl� primaire : 9) ?");
							affiche = sc.nextInt();
						} while (affiche != 1 && affiche != 2 && affiche != 3 && affiche != 4 && affiche != 5 && affiche != 6 && affiche != 7 && affiche != 8 && affiche != 9);
						List<Client> lc = new ArrayList<Client>();
						if (affiche == 1) {
							daos.getClientDAO().show();
						}
						else if (affiche == 2) {
							videur = sc.nextLine();
							System.out.println("nom : ");
							nom = sc.nextLine();
							
							lc = daos.getClientDAO().getClientByNom(nom);
						}
						else if (affiche == 3) {
							videur = sc.nextLine();
							System.out.println("prenom : ");
							prenom = sc.nextLine();
							
							lc = daos.getClientDAO().getClientByPrenom(prenom);
						}
						else if (affiche == 4) {
							videur = sc.nextLine();
							System.out.println("no_rue : ");
							no_rue = sc.nextLine();
							
							lc = daos.getClientDAO().getClientByNoRue(no_rue);
						}
						else if (affiche == 5) {
							videur = sc.nextLine();
							System.out.println("voie : ");
							voie = sc.nextLine();
							
							lc = daos.getClientDAO().getClientByVoie(voie);
						}
						else if (affiche == 6) {
							videur = sc.nextLine();
							System.out.println("code_postal : ");
							code_postal = sc.nextLine();
							
							lc = daos.getClientDAO().getClientByCodePostal(code_postal);
						}
						else if (affiche == 7) {
							videur = sc.nextLine();
							System.out.println("ville : ");
							ville = sc.nextLine();
							
							lc = daos.getClientDAO().getClientByVille(ville);
						}
						else if (affiche == 8) {
							videur = sc.nextLine();
							System.out.println("pays : ");
							pays = sc.nextLine();
							
							lc = daos.getClientDAO().getClientByPays(pays);
						}
						else if (affiche == 9) {
							System.out.println("id_client : ");
							id_client = sc.nextInt();
							
							System.out.println(daos.getClientDAO().getById(id_client).toString());
						}
						if (affiche != 1 && affiche != 9) {
							for (int i=0; i<lc.size(); i++) {
								System.out.println(lc.get(i).getIdcl() + " " + lc.get(i).getNom() + " " + lc.get(i).getPrenom() + " " + lc.get(i).getNorue() + " " + lc.get(i).getVoie() + " " + lc.get(i).getCodepostal() + " " + lc.get(i).getVille() + " " + lc.get(i).getPays());
							}
						}
					}
					else if (action == 5) {
						System.out.println("Nom du fichier (sans l'extension) : ");
						String nomFichier = sc.nextLine();
						
						daos.getClientDAO().setDataByCSV(nomFichier);
					}
					do {
						System.out.println("Continuer les action sur cette table :");
						System.out.println("(Oui : 1; Non : 2) :");
						action_continuer = sc.nextInt();
					} while (action_continuer != 1 && action_continuer != 2);
				} while(action_continuer == 1);
			}
			else if (table == 3) {
				do {
					do {
						System.out.println("Quelle action voulez-vous realiser :");
						System.out.println("(Ajouter une ligne : 1; Modifier une ligne : 2; Supprimer une ligne : 3; Afficher : 4) ?");
						action = sc.nextInt();
					} while (action != 1 && action != 2 && action != 3 && action != 4);
					if (action == 1) {
						System.out.println("Veuillez saisir les parametres requis :");
						videur = sc.nextLine();
						System.out.println("libelle : ");
						libelle = sc.nextLine();
						
						daos.getPeriodiciteDAO().create(new Periodicite(0, libelle));
					}
					else if (action == 2) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_periodicite : ");
						id_periodicite = sc.nextInt();
						videur = sc.nextLine();
						System.out.println("libelle : ");
						libelle = sc.nextLine();
						
						daos.getPeriodiciteDAO().update(new Periodicite(id_periodicite, libelle));
					}
					else if (action == 3) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_periodicite : ");
						id_periodicite = sc.nextInt();
						
						daos.getPeriodiciteDAO().delete(new Periodicite(id_periodicite, null));
					}
					else if (action == 4) {
						do {
							System.out.println("Que voulez-vous afficher :");
							System.out.println("(La table : 1; Par libelle : 2; Par cl� pimaire : 3) ?");
							affiche = sc.nextInt();
						} while (affiche != 1 && affiche != 2 && affiche != 3);
						List<Periodicite> lp = new ArrayList<Periodicite>();
						if (affiche == 1) {
							daos.getPeriodiciteDAO().show();
						}
						else if (affiche == 2) {
							videur = sc.nextLine();
							System.out.println("libelle : ");
							libelle = sc.nextLine();
							
							lp = daos.getPeriodiciteDAO().getPeriodiciteByLibelle(libelle);
						}
						else if (affiche == 2) {
							System.out.println("id_periodicite : ");
							id_periodicite = sc.nextInt();
							
							System.out.println(daos.getPeriodiciteDAO().getById(id_periodicite).toString());
						}
						if (affiche != 1 && affiche != 3) {
							for (int i=0; i<lp.size(); i++) {
								System.out.println(lp.get(i).getIdPeriodicite() + " " + lp.get(i).getLibelle());
							}
						}
					}
					do {
						System.out.println("Continuer les action sur cette table :");
						System.out.println("(Oui : 1; Non : 2) :");
						action_continuer = sc.nextInt();
					} while (action_continuer != 1 && action_continuer != 2);
				} while(action_continuer == 1);
			}
			else if (table == 4) {
				do {
					do {
						System.out.println("Quelle action voulez-vous realiser :");
						System.out.println("(Ajouter une ligne : 1; Modifier une ligne : 2; Supprimer une ligne : 3; Afficher : 4) ?");
						action = sc.nextInt();
					} while (action != 1 && action != 2 && action != 3 && action != 4);
					if (action == 1) {
						System.out.println("Veuillez saisir les parametres requis :");
						videur = sc.nextLine();
						System.out.println("titre : ");
						titre = sc.nextLine();
						System.out.println("description : ");
						description = sc.nextLine();
						System.out.println("tarif_numero : ");
						tarif_numero = sc.nextDouble();
						videur = sc.nextLine();
						System.out.println("visuel : ");
						visuel = sc.nextLine();
						System.out.println("id_periodicite : ");
						id_periodicite = sc.nextInt();
						
						daos.getRevueDAO().create(new Revue(0, titre, description, (float) tarif_numero, visuel, id_periodicite));
					}
					else if (action == 2) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_revue : ");
						id_revue = sc.nextInt();
						videur = sc.nextLine();
						System.out.println("titre : ");
						titre = sc.nextLine();
						System.out.println("description : ");
						description = sc.nextLine();
						System.out.println("tarif_numero : ");
						tarif_numero = sc.nextDouble();
						videur = sc.nextLine();
						System.out.println("visuel : ");
						visuel = sc.nextLine();
						System.out.println("id_periodicite : ");
						id_periodicite = sc.nextInt();
						
						daos.getRevueDAO().update(new Revue(id_revue, titre, description, (float) tarif_numero, visuel, id_periodicite));
					}
					else if (action == 3) {
						System.out.println("Veuillez saisir les parametres requis :");
						System.out.println("id_revue : ");
						id_revue = sc.nextInt();
						
						daos.getRevueDAO().delete(new Revue(id_revue, null, null, (float) 0, null, 0));
					}
					else if (action == 4) {
						do {
							System.out.println("Que voulez-vous afficher :");
							System.out.println("(La table : 1; Par titre : 2; Par description : 3; Par tarif : 4; Par visuel : 5; Par numero de periodicite : 6; Par cl� primaire : 7) ?");
							affiche = sc.nextInt();
						} while (affiche != 1 && affiche != 2 && affiche != 3 && affiche != 4 && affiche != 5 && affiche != 6 && affiche != 7);
						List<Revue> lr = new ArrayList<Revue>();
						if (affiche == 1) {
							daos.getRevueDAO().show();
						}
						else if (affiche == 2) {
							videur = sc.nextLine();
							System.out.println("titre : ");
							titre = sc.nextLine();
							
							lr = daos.getRevueDAO().getByTitre(titre);
						}
						else if (affiche == 3) {
							videur = sc.nextLine();
							System.out.println("description : ");
							description = sc.nextLine();
							
							lr = daos.getRevueDAO().getByDescription(description);
						}
						else if (affiche == 4) {
							System.out.println("tarif : ");
							tarif_numero = sc.nextFloat();
							
							lr = daos.getRevueDAO().getByTarifNumero((float) tarif_numero);
						}
						else if (affiche == 5) {
							videur = sc.nextLine();
							System.out.println("visuel : ");
							visuel = sc.nextLine();
							
							lr = daos.getRevueDAO().getByVisuel(visuel);
						}
						else if (affiche == 6) {
							System.out.println("id de periodicite : ");
							id_periodicite = sc.nextInt();
							
							lr = daos.getRevueDAO().getByIdPeriodicite(id_periodicite);
						}
						else if (affiche == 7) {
							System.out.println("id_revue : ");
							id_revue = sc.nextInt();
							
							System.out.println(daos.getRevueDAO().getById(id_revue).toString());
						}
						if (affiche != 1 && affiche != 7) {
							for (int i=0; i<lr.size(); i++) {
								System.out.println(lr.get(i).getIdRevue() + " " + lr.get(i).getTitre() + " " + lr.get(i).getDescription() + " " + lr.get(i).getTarifNumero() + " " + lr.get(i).getVisuel() + " " + lr.get(i).getIdPeriodicite());
							}
						}
					}
					do {
						System.out.println("Continuer les action sur cette table :");
						System.out.println("(Oui : 1; Non : 2) :");
						action_continuer = sc.nextInt();
					} while (action_continuer != 1 && action_continuer != 2);
				} while(action_continuer == 1);
			}
			System.out.println("Voulez-vous continuer :");
			System.out.println("(Oui : 1; Non : 2) ?");
			continuer = sc.nextInt();
		}
	}

}