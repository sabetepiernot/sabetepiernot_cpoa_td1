package metier;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Periodicite {
	private int idPeriodicite;
	private String libelle;

	public Periodicite(int idPeriodicite, String libelle) {
		this.idPeriodicite = idPeriodicite;
		this.libelle = libelle;
	}

	public int getIdPeriodicite() {
		return idPeriodicite;
	}

	public void setIdPeriodicite(int idPeriodicite) {
		this.idPeriodicite = idPeriodicite;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	
	public boolean idPeriodiciteEquals(Periodicite p) {
		return (this.idPeriodicite == p.idPeriodicite);
	}
	
	public boolean libelleEquals(Periodicite p) {
		return (this.libelle.equals(p.libelle));
	}

}

// CTRL + SHIFT + O -> import
// CTRL + SHIFT + F -> Formate le code