package normalisation;

public abstract class MethodesCommunes {
	
	public static String removeSpaceBeginEnd(String str) {
		return str.trim();
	}
	
	public static String removeSpaceInside(String str) {
		String result = "";
		if (str.length() > 0) {
			for (int i=0; i<str.length()-1; i++) {
				if (!(str.charAt(i) == str.charAt(i+1) && str.charAt(i) == ' ')) {
					result = result + str.charAt(i);
				}
			}
			result = result + str.charAt(str.length()-1);
		}
		return result;
	}
}
