package normalisation;

public class NoRue extends MethodesCommunes {
	private String nomRue;
	
	public NoRue(String nomRue) {
		this.nomRue = this.nomRueNormalisation(nomRue);
	}
	
	public static String nomRueNormalisation(String nomRue) {
		nomRue = removeSpaceBeginEnd(nomRue);
		nomRue = removeSpaceInside(nomRue);
		nomRue = nomRueAbrege(nomRue);
		return nomRue;
	}
	
	public static String nomRueAbrege(String nomRue) {
		String result = nomRue.toLowerCase();
		String[] parts = result.split(" ");
		result = "";
		for (int i=0; i<parts.length; i++) {
			if ((parts[i].equals("boul.")) || (parts[i].equals("bd")) || (parts[i].equals("boul"))) {
				result += "boulevard";
			}
			else if (parts[i].equals("av.")) {
				result += "avenue";
			}
			else if ((parts[i].equals("faub.")) || (parts[i].equals("fg"))) {
				result += "faubourg";
			}
			else if (parts[i].equals("pl.")) {
				result += "place";
			}
			else {
				result += parts[i];
			}
			
			if (i != parts.length-1) {
				result += " ";
			}
		}
		
		return result;
	}
}
