package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import normalisation.CodePostal;
import normalisation.NoRue;

public class CodePostalTest {

	
	
	
	@Test
	public void testAjoutzero() {
		assertEquals("08200", CodePostal.Ajoutzero("8200"));
	}
	
	
	@Test
	public void testAjoutzeroTrois() {
		assertEquals("00570", CodePostal.Ajoutzero("570"));
	}
	
	
	@Test
	public void testAjoutzeroDeux() {
		assertEquals("00045", CodePostal.Ajoutzero("45"));
	
	}
	
	@Test
	public void testAjoutzeroUn() {
		assertEquals("00001", CodePostal.Ajoutzero("1"));
	
	}
	
	@Test
	public void testAjoutzeroVide() {
		assertEquals("00000", CodePostal.Ajoutzero(""));
	}
	
	
	
	@Test
	public void testIndpays() {
		assertEquals("3300", CodePostal.Indpays("   B-3300"));
	}
	
		
	
	@Test
	public void testIndpaysVide() {
		assertEquals("67000", CodePostal.Indpays("67000"));
	}
	
	
	@Test
	public void testCodePostalNormalisation() {
		assertEquals("00057", CodePostal.codepostalNormalisation("57"));
	}
	
	@Test
	public void testCodePostalNormalisationIndPays() {
		assertEquals("03300", CodePostal.codepostalNormalisation("B-3300"));
	}
	
	

}
