package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import normalisation.MethodesCommunes;

public class MethodesCommunesTest {
	
	//Test sur removeSpaceBeginEnd
	@Test
	public void testRemoveSpaceEnd() {
		assertEquals("test", MethodesCommunes.removeSpaceBeginEnd("test  "));
	}
	
	@Test
	public void testRemoveSpaceBegin() {
		assertEquals("test", MethodesCommunes.removeSpaceBeginEnd(" test"));
	}
	
	@Test
	public void testRemoveSpaceBeginEnd() {
		assertEquals("test", MethodesCommunes.removeSpaceBeginEnd("  test  "));
	}
	
	@Test
	public void testNotRemoveSpace() {
		assertEquals("test", MethodesCommunes.removeSpaceBeginEnd("test"));
	}
	
	@Test
	public void testChaineVide() {
		assertEquals("", MethodesCommunes.removeSpaceBeginEnd(""));
	}
	
	
	//Test sur removeSpaceInside
	@Test
	public void testRemoveSpaceInside() {
		assertEquals("t e st", MethodesCommunes.removeSpaceInside("t e   st"));
	}
	
	@Test
	public void testRemoveNoneSpaceInside() {
		assertEquals("t e st", MethodesCommunes.removeSpaceInside("t e st"));
	}
	
	@Test
	public void testRemoveSpaceWithoutSpace() {
		assertEquals("test", MethodesCommunes.removeSpaceInside("test"));
	}
	
	@Test
	public void testRemoveSpaceWithChaineVide() {
		assertEquals("", MethodesCommunes.removeSpaceInside(""));
	}

}
