package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import normalisation.NoRue;

public class NoRueTest {
	
	//test nomRueAbrege
	@Test
	public void testNomRue() {
		assertEquals("boulevard des coques", NoRue.nomRueAbrege("boul des coques"));
	}
	
	@Test
	public void testNomRueWithOtherAbrege() {
		assertEquals("boulevard faubourg faubourg boulevard avenue place boulevard", NoRue.nomRueAbrege("boul faub. fg boul. av. pl. bd"));
	}

	@Test
	public void testNomRueWithChaineVide() {
		assertEquals("", NoRue.nomRueAbrege(""));
	}

	@Test
	public void testNomRueWithoutAbrege() {
		assertEquals("pont des morts", NoRue.nomRueAbrege("pont des morts"));
	}
	
	
	//test nomRueNormalisation
	@Test
	public void testNomRueNormalisation() {
		assertEquals("boulevard de avenue coques", NoRue.nomRueNormalisation("    Boul de  av.  coques "));
	}
	
	@Test
	public void testNomRueNormalisationChaineVide() {
		assertEquals("", NoRue.nomRueNormalisation(""));
	}
	
	@Test
	public void testNomRueNormalisationWithOnlySpace() {
		assertEquals("", NoRue.nomRueNormalisation("     "));
	}
}
