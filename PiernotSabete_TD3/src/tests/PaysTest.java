package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import normalisation.NoRue;
import normalisation.Pays;

public class PaysTest {

	@Test
	public void testMinuscule() {
		assertEquals("Belgique", Pays.Minuscule("BELGIQUE"));
	}
	
	@Test
	public void testMajuscule() {
		assertEquals("Allemagne", Pays.Majuscule("allemagne"));
	}
	
	@Test
	public void testFranciser() {
		assertEquals("Suisse", Pays.Franciser("Schweiz"));
	}
	

	//paysNormalisation
	@Test
	public void paysNormalisation() {
		assertEquals("Suisse", Pays.paysNormalisation(" scHweiz   "));
	}
}
