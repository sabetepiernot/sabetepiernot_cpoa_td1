package controleur;

import java.util.ArrayList;
import java.util.Scanner;

import enumerations.EPersistance;
import factory.DAOFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import metier.Periodicite;
import metier.Revue;

public class MainControleur implements EventHandler<ActionEvent> {
	@FXML
	private Label lbl_resultat;
	@FXML
	private RadioButton rb_mysql;
	@FXML
	private RadioButton rb_lm;
	@FXML
	private ComboBox<Periodicite> cb_periodicite;
	@FXML
	private TextField edt_titre;
	@FXML
	private TextField edt_description;
	@FXML
	private TextField edt_tarif;
	
	private DAOFactory daos = null;
	private ArrayList<Periodicite> liste;
	
	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void creerModele() {
		int bon = 1;
		float tarif = 0;
		try {
			tarif = Float.parseFloat(this.edt_tarif.getText());
		}
		catch (NumberFormatException e) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le tarif");
			bon = 0;
		}
		if (this.edt_titre.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le titre");
		}
		else if (this.edt_description.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour la description");
		}
		else if (this.edt_tarif.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le tarif");
		}
		else {
			if (bon == 1) {
				String titre = this.edt_titre.getText();
				String desc = this.edt_description.getText();
				try {
					int idPerio = this.cb_periodicite.getValue().getIdPeriodicite();
					daos.getRevueDAO().create(new Revue(0,titre,desc,(float) tarif,"",idPerio));
					this.lbl_resultat.setText(titre+" ("+tarif+" euros)");
				}
				catch (Exception e) {
					this.lbl_resultat.setText("Veuillez selectionner un �l�ment pour la p�riodicit�");
				}
				
			}
		}
		
		
		
	}
	
	public void clickMySQL() {
		this.rb_mysql.setSelected(true);
		this.rb_lm.setSelected(false);
		
		daos = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		liste = daos.getPeriodiciteDAO().findAll();
		ObservableList<Periodicite> options = FXCollections.observableArrayList(liste);
		cb_periodicite.setItems(options);
	}
	
	public void clickListeMemoire() {
		this.rb_mysql.setSelected(false);
		this.rb_lm.setSelected(true);
		
		daos = DAOFactory.getDAOFactory(EPersistance.MEMORY);
		liste = daos.getPeriodiciteDAO().findAll();
		ObservableList<Periodicite> options = FXCollections.observableArrayList(liste);
		cb_periodicite.setItems(options);
	}
	
	public void initPeriodicite() {
		if (daos == null) {
			daos = DAOFactory.getDAOFactory(EPersistance.MYSQL);
		}
		liste = daos.getPeriodiciteDAO().findAll();
		ObservableList<Periodicite> options = FXCollections.observableArrayList(liste);
		cb_periodicite.setItems(options);
	}

}
