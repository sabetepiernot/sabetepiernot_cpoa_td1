package interfaces;

import java.sql.Date;
import java.util.List;

import metier.Abonnement;

public interface IAbonnementDAO extends IDAO<Abonnement> {

	List<Abonnement> getAbonnementByIdClient(int idClient);
	List<Abonnement> getAbonnementByIdRevue(int idRevue);
	List<Abonnement> getAbonnementByDateDebut(Date dateDebut);
	List<Abonnement> getAbonnementByDateFin(Date dateFin);
}
