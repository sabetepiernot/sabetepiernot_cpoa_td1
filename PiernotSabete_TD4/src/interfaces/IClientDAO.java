package interfaces;

import java.io.IOException;
import java.util.List;

import metier.Client;

public interface IClientDAO extends IDAO<Client> {

	List<Client> getClientByNom(String nom);
	List<Client> getClientByPrenom(String prenom);
	List<Client> getClientByNoRue(String noRue);
	List<Client> getClientByVoie(String voie);
	List<Client> getClientByCodePostal(String cp);
	List<Client> getClientByVille(String ville);
	List<Client> getClientByPays(String pays);
	void setDataByCSV(String fileName) throws IOException;
}
