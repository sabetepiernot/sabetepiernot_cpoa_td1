package interfaces;

public interface IDAO<T> {

	public abstract boolean create(T obj);

	public abstract boolean delete(T obj);

	public abstract boolean update(T obj);

	public abstract boolean show();

	public abstract T getById(int id);
}