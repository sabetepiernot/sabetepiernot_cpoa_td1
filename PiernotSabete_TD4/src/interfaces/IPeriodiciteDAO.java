package interfaces;

import java.util.ArrayList;
import java.util.List;

import metier.Periodicite;

public interface IPeriodiciteDAO extends IDAO<Periodicite> {

	List<Periodicite> getPeriodiciteByLibelle(String libelle);

	ArrayList<Periodicite> findAll();
}
