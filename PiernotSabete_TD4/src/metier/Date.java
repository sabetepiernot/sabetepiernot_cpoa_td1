package metier;

public class Date {
	private int jour;
	private int mois;
	private int annee;

	public Date(int j, int m, int a) {
		this.jour = j;
		this.mois = m;
		this.annee = a;
	}

	public int getJour() {
		return jour;
	}

	public int getMois() {
		return mois;
	}

	public int getAnnee() {
		return annee;
	}

	public void setJour(int jour) {
		this.jour = jour;
	}

	public void setMois(int mois) {
		this.mois = mois;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

}