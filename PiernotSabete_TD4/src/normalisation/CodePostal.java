package normalisation;

public class CodePostal extends MethodesCommunes {

	private String codepostal;
	
	public CodePostal(String cp){
		
		this.codepostal=this.codepostalNormalisation(cp);
		
	}
	
	 public static String codepostalNormalisation(String cp) {
			cp = removeSpaceBeginEnd(cp);
			cp = removeSpaceInside(cp);
			cp=Indpays(cp);
			cp = Ajoutzero(cp);
			return cp;
		}
	
	
	//Ajout du 0
	

	public static String Ajoutzero(String cp)
	{
		
		int lgcp=cp.length();

		switch(lgcp){
		case 0:
			return cp=("00000"+cp);
		case 1:
			return cp=("0000"+cp);
		case 2:
			return cp=("000"+cp);
		case 3:
			return cp=("00"+cp);
		case 4:
			return cp=("0"+cp);
		default :
			return cp;
		}
			
		}

	
	
	// Suppression des indicatifs pays
	
	public static String Indpays(String cp)
	{

		
		
		if((cp.startsWith("L-"))||(cp.startsWith("B-"))||cp.startsWith("S-"))
		{
		String c=cp.substring(2,6);
		return c;
		}
		else return Ajoutzero(cp);
	
		
	}
	
	

	
	
	
}
