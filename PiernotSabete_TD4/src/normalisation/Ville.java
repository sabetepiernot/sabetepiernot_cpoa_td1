package normalisation;

import tests.VilleTest;

public class Ville extends MethodesCommunes {
	private String ville;
	
	public Ville(String ville) {
		this.ville = this.villeNormalisation(ville);
	}
	
	public static String villeNormalisation(String ville) {
		ville = removeSpaceBeginEnd(ville);
		ville = removeSpaceInside(ville);
		ville = villeAbrege(ville);
		ville = villeMajuscule(ville);
		ville = villeTiret(ville);
		return ville;
	}
	
	public static String villeAbrege(String ville) {
		String result = ville.toLowerCase();
		String[] parts = result.split(" ");
		result = "";
		for (int i=0; i<parts.length; i++) {
			if (parts[i].equals("st")) {
				result += "saint";
			}
			else if (parts[i].equals("ste")) {
				result += "sainte";
			}
			else {
				result += parts[i];
			}
			
			if (i != parts.length-1) {
				result += " ";
			}
		}
		
		return result;
	}
	
	public static String villeMajuscule(String ville) {
		String result = ville.toLowerCase();
		String[] parts = result.split(" ");
		result = "";
		for (int i=0; i<parts.length; i++) {
			try {
				if (!((parts[i].equals("le")) || (parts[i].equals("l�s")) || (parts[i].equals("sous")) || (parts[i].equals("sur")) || (parts[i].equals("�")) || (parts[i].equals("aux")))){
					parts[i] = parts[i].replaceFirst(".", (parts[i].charAt(0)+"").toUpperCase());
				}
			} catch (Exception e) {
				return "";
			}
			result += parts[i];
			if (i != parts.length-1) {
				result += " ";
			}
		}
		
		return result;
	}
	
	public static String villeTiret(String ville) {
		String result = ville;
		String[] parts = result.split(" ");
		result = "";
		boolean tiret;
		for (int i=0; i<parts.length; i++) {
			tiret = false;
			if ((parts[i].equals("Sainte")) || (parts[i].equals("Saint")) || (parts[i].equals("le")) || (parts[i].equals("l�s")) || (parts[i].equals("sous")) || (parts[i].equals("sur")) || (parts[i].equals("�")) || (parts[i].equals("aux"))) {
				tiret = true;
			}
			
			if (i != parts.length-1) {
				if (tiret) {
					result = result.substring(0, result.length()-1) + "-" + parts[i] + "-";
				}
				else {
					result += parts[i] + " ";
				}
			}
			else {
				if (tiret) {
					result = result.substring(0, result.length()-1) + "-" + parts[i];
				}
				else {
					result += parts[i];
				}
			}
		}
		result = removeSpaceBeginEnd(result);
		
		return result;
	}
}
