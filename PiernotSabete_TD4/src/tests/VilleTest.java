package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import normalisation.Ville;

public class VilleTest {

	//testvilleAbrege
	@Test
	public void testvilleAbrege() {
		assertEquals("sainte saint sainte saint longwy metz marseille", Ville.villeAbrege("Ste st ste St Longwy Metz Marseille"));
	}
	
	@Test
	public void testvilleAbregeChaineVide() {
		assertEquals("", Ville.villeAbrege(""));
	}
	
	@Test
	public void testvilleAbregeSansModif() {
		assertEquals("longwy", Ville.villeAbrege("longwy"));
	}
	
	
	//villeMajuscule
	@Test
	public void villeMajuscule() {
		assertEquals("Longwy", Ville.villeMajuscule("longwy"));
	}
	
	@Test
	public void villeMajusculeLong() {
		assertEquals("Longwy Metz Lyon Florence", Ville.villeMajuscule("longwY mEtz Lyon florence"));
	}
	
	@Test
	public void villeMajusculeChaineVide() {
		assertEquals("", Ville.villeMajuscule(""));
	}
	
	@Test
	public void villeMajusculeAvecMotsSpecials() {
		assertEquals("Saint Julien l�s Metz", Ville.villeMajuscule("saint julien l�s MetZ"));
	}
	
	
	//villeTiret
	@Test
	public void villeTiretUn() {
		assertEquals("Longwy-Saint-Mont", Ville.villeTiret("Longwy Saint Mont"));
	}
	
	@Test
	public void villeTiretDeux() {
		assertEquals("Longwy-Saint-Mont Arbre-Sainte-Vierge-Saint", Ville.villeTiret("Longwy Saint Mont Arbre Sainte Vierge Saint"));
	}
	
	@Test
	public void villeTiretPasDeChangement() {
		assertEquals("Longwy", Ville.villeTiret("Longwy"));
	}
	
	@Test
	public void villeTiretVide() {
		assertEquals("", Ville.villeTiret(""));
	}
	
	
	//villeNormalisation
	@Test
	public void villeNormalisation() {
		assertEquals("Longwy-Saint-Mont", Ville.villeNormalisation("   Longwy   St Mont  "));
	}
	
	@Test
	public void villeNormalisationComplexe() {
		assertEquals("Longwy-Saint-l�s-Monts Arbre-Sainte-Vierge-Saint", Ville.villeNormalisation("  Longwy  Saint   l�s Monts   Arbre Sainte  Vierge Saint "));
	}
	
	@Test
	public void villeNormalisationSimple() {
		assertEquals("Longwy", Ville.villeNormalisation("longwy"));
	}
	
	@Test
	public void villeNormalisationVide() {
		assertEquals("", Ville.villeNormalisation(""));
	}
}
