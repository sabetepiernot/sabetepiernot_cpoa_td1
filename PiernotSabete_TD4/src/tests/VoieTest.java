package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import normalisation.Pays;
import normalisation.Voie;

public class VoieTest {

	@Test
	public void testVirgulevoie1chiffre() {
		assertEquals("3,", Voie.Virgulevoie("3"));
	}
	
	@Test
	public void testVirgulevoie2chiffres() {
		assertEquals("12,", Voie.Virgulevoie("12"));
	}
	
	@Test
	public void testVirgulevoie3chiffres() {
		assertEquals("200,", Voie.Virgulevoie("200"));
	}
	
	
	@Test
	public void testVoieNormalisation() {
		assertEquals("2,", Voie.voieNormalisation("2"));
	}
	
	@Test
	public void testVoieNormalisationVide() {
		assertEquals("", Voie.voieNormalisation(""));
	}


}
