Groupe 7 : PIERNOT Alexis et SABETE Navid

Ce qui fonctionne :
- le menu et le choix de la DAO
- l'affichage, l'ajout, la modification et la suppression d'une p�riodicit�, d'un client, d'une revue et d'un abonnement

Ce qu'il n'y a pas :
- l'ajout d'un fichier csv
- le renouvellement d'un abonnement
- l'affichage du visuel d'une revue

R�partition des t�ches entre les membres du groupe:
- Alexis : Menu; Tout concernant les p�riodicit�s; Tout concernant les revues; 70 % concernant les abonnement.
- Navid : Tout concernant les clients; 30 % concernant les abonnements.

Estimation du pourcentage de l'investissement de chacun dans le projet rendu :
- Selon PIERNOT Alexis :
	Alexis 70 %
	Navid 30 %
- Selon SABETE Navid :
	Alexis 55 %
	Navid 45 %