package controleur;

import java.io.IOException;

import enumerations.EPersistance;
import factory.DAOFactory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import metier.Abonnement;
import metier.Date;
import metier.Periodicite;
import vue.VueAbonnement;
import vue.VueAjouterAbonnement;
import vue.VueModifierAbonnement;
import vue.VueSupprimerAbonnement;

public class AfficherAbonnementControleur implements EventHandler<ActionEvent>, ChangeListener<Abonnement> {
	@FXML
	private TableView<Abonnement> tab_abo;
	@FXML
	private Button btn_ajouter;
	@FXML
	private Button btn_modifier;
	@FXML
	private Button btn_supprimer;
	@FXML
	private Label lbl_erreur;

	private Abonnement aSelected = null;

	private DAOFactory daos;

	private VueAbonnement vue;

	@FXML
	public void initTabAbo() {
		//Evenement
		this.tab_abo.getSelectionModel().selectedItemProperty().addListener(this);
		
		// Cr�ation colonnes
		TableColumn<Abonnement, Integer> colClient = new TableColumn<Abonnement, Integer>("Client");
		TableColumn<Abonnement, Integer> colRevue = new TableColumn<Abonnement, Integer>("Revue");
		TableColumn<Abonnement, String> colDateDebut = new TableColumn<Abonnement, String>("Date d�but");
		TableColumn<Abonnement, String> colDateFin = new TableColumn<Abonnement, String>("Date fin");

		colClient.setCellValueFactory(new PropertyValueFactory<Abonnement, Integer>("idclient"));
		colRevue.setCellValueFactory(new PropertyValueFactory<Abonnement, Integer>("idrevue"));
		colDateDebut.setCellValueFactory(new PropertyValueFactory<Abonnement, String>("dd"));
		colDateFin.setCellValueFactory(new PropertyValueFactory<Abonnement, String>("df"));

		this.tab_abo.getColumns().setAll(colClient, colRevue, colDateDebut, colDateFin);

		this.tab_abo.getItems().addAll(daos.getAbonnementDAO().findAll());

		this.lbl_erreur.setText("");

	}

	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void setVue(VueAbonnement vueAbonnement) {
		this.vue = vueAbonnement;
	}

	public void clickAjouter() throws IOException {
		new VueAjouterAbonnement(daos, this);
	}

	public void clickModifier() throws IOException {
		new VueModifierAbonnement(daos, this, aSelected);
	}

	public void clickSupprimer() throws IOException {
		new VueSupprimerAbonnement(daos, this, aSelected);
	}

	@Override
	public void changed(ObservableValue<? extends Abonnement> observable, Abonnement oldValue, Abonnement newValue) {
		// TODO Auto-generated method stub
		this.btn_modifier.setDisable(newValue == null);
		this.btn_supprimer.setDisable(newValue == null);
		
		if (newValue != null) aSelected = newValue;
	}
	
	public void enterAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterModifier() {
		if (aSelected != null) this.btn_modifier.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitModifier() {
		if (aSelected != null) this.btn_modifier.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterSupprimer() {
		if (aSelected != null) this.btn_supprimer.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitSupprimer() {
		if (aSelected != null) this.btn_supprimer.setStyle("-fx-background-color:  #74B8FF");
	}

	public TableView<Abonnement> getTab_abo() {
		return tab_abo;
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public VueAbonnement getVue() {
		return vue;
	}

	public void setTab_abo(TableView<Abonnement> tab_abo) {
		this.tab_abo = tab_abo;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

}
