package controleur;

import java.io.IOException;

import enumerations.EPersistance;
import factory.DAOFactory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import metier.Client;
import vue.VueAjouterClient;
import vue.VueClient;
import vue.VueModifierClient;
import vue.VueSupprimerClient;

public class AfficherClientControleur implements EventHandler<ActionEvent>, ChangeListener<Client> {
	@FXML
	private TableView<Client> tab_client;

	@FXML
	private Button btn_ajouter;
	@FXML
	private Button btn_modifier;
	@FXML
	private Button btn_supprimer;

	@FXML
	private Label lbl_erreur;

	private Client cSelected = null;
	private DAOFactory daos;

	private VueClient vue;

	public void initial() {
		// Evenement
		this.tab_client.getSelectionModel().selectedItemProperty().addListener(this);
		
		// Cr�ation colonnes

		// fx:id="nom"
		TableColumn<Client, String> colNom = new TableColumn<Client, String>("Nom");

		// fx:id="prenom"
		TableColumn<Client, String> colPrenom = new TableColumn<Client, String>("Pr�nom");

		// fx:id="norue"
		TableColumn<Client, String> colNoRue = new TableColumn<Client, String>("N� Rue");

		// fx:id="voie"
		TableColumn<Client, String> colVoie = new TableColumn<Client, String>("Voie");

		// fx:id="code_postal"
		TableColumn<Client, String> colCodePostal = new TableColumn<Client, String>("Code Postal");

		// fx:id="ville"
		TableColumn<Client, String> colVille = new TableColumn<Client, String>("Ville");

		// fx:id="pays"
		TableColumn<Client, String> colPays = new TableColumn<Client, String>("Pays");

		colNom.setCellValueFactory(new PropertyValueFactory<Client, String>("nom"));

		colPrenom.setCellValueFactory(new PropertyValueFactory<Client, String>("prenom"));

		colNoRue.setCellValueFactory(new PropertyValueFactory<Client, String>("norue"));
		colVoie.setCellValueFactory(new PropertyValueFactory<Client, String>("voie"));

		colCodePostal.setCellValueFactory(new PropertyValueFactory<Client, String>("codepostal"));

		colVille.setCellValueFactory(new PropertyValueFactory<Client, String>("ville"));

		colPays.setCellValueFactory(new PropertyValueFactory<Client, String>("pays"));

		this.tab_client.getColumns().setAll(colNom, colPrenom, colNoRue, colVoie, colCodePostal, colVille, colPays);

		this.tab_client.getItems().addAll(daos.getClientDAO().findAll());
		this.lbl_erreur.setText("");

	}

	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void setVue(VueClient vueClient) {
		this.vue = vueClient;
	}

	public void openAjoutClient() throws IOException {
		new VueAjouterClient(daos, this);
	}

	public void openModifierClient() throws IOException {
		new VueModifierClient(daos, this, cSelected);
	}

	public void openSupprimerClient() throws IOException {
		new VueSupprimerClient(daos, this, cSelected); // n'arrive pas � s'ouvrir
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public TableView<Client> getTab_client() {
		return tab_client;
	}

	public void setTab_client(TableView<Client> tab_client) {
		this.tab_client = tab_client;
	}

	@Override
	public void changed(ObservableValue<? extends Client> observable, Client oldValue, Client newValue) {
		// TODO Auto-generated method stub
		this.btn_modifier.setDisable(newValue == null);
		this.btn_supprimer.setDisable(newValue == null);

		if (newValue != null)
			cSelected = newValue;
	}
	
	public void enterAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterModifier() {
		if (cSelected != null) this.btn_modifier.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitModifier() {
		if (cSelected != null) this.btn_modifier.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterSupprimer() {
		if (cSelected != null) this.btn_supprimer.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitSupprimer() {
		if (cSelected != null) this.btn_supprimer.setStyle("-fx-background-color:  #74B8FF");
	}
}
