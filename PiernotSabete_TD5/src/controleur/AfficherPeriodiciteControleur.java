package controleur;

import java.io.IOException;

import enumerations.EPersistance;
import factory.DAOFactory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import metier.Periodicite;
import vue.VueAjouterPeriodicite;
import vue.VueAjouterRevue;
import vue.VueModifierPeriodicite;
import vue.VueModifierRevue;
import vue.VuePeriodicite;
import vue.VueSupprimerPeriodicite;
import vue.VueSupprimerRevue;

public class AfficherPeriodiciteControleur implements EventHandler<ActionEvent>, ChangeListener<Periodicite> {
	@FXML
	private TableView<Periodicite> tab_periodicite;
	@FXML
	private Button btn_ajouter;
	@FXML
	private Button btn_modifier;
	@FXML
	private Button btn_supprimer;
	@FXML
	private Label lbl_erreur;
	
	private DAOFactory daos;
	
	private Periodicite pSelected = null;
	
	private VuePeriodicite vue;
	
	public void initial() {
		//Evenement
		this.tab_periodicite.getSelectionModel().selectedItemProperty().addListener(this);

		// Cr�ation colonnes
		TableColumn<Periodicite, String> colLibelle = new TableColumn<Periodicite, String>("Libelle");
		colLibelle.setCellValueFactory(new PropertyValueFactory<Periodicite, String>("libelle"));
		colLibelle.setStyle("-fx-min-width : 200; -fx-pref-width : 200");

		this.tab_periodicite.getColumns().setAll(colLibelle);
		this.tab_periodicite.getItems().addAll(daos.getPeriodiciteDAO().findAll());
		
	}

	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void setVue(VuePeriodicite vuePeriodicite) {
		this.vue = vuePeriodicite;
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	@Override
	public void changed(ObservableValue<? extends Periodicite> observable, Periodicite oldValue, Periodicite newValue) {
		// TODO Auto-generated method stub
		this.btn_modifier.setDisable(newValue == null);
		this.btn_supprimer.setDisable(newValue == null);
		
		if (newValue != null) pSelected = newValue;
	}

	
	public void clickAjouter() throws IOException {
		new VueAjouterPeriodicite(daos, this);
	}
	
	public void clickModifier() throws IOException {
		new VueModifierPeriodicite(daos, this, pSelected);
	}
	
	public void clickSupprimer() throws IOException {
		new VueSupprimerPeriodicite(daos, this, pSelected);
	}
	
	public void enterAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterModifier() {
		if (pSelected != null) this.btn_modifier.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitModifier() {
		if (pSelected != null) this.btn_modifier.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterSupprimer() {
		if (pSelected != null) this.btn_supprimer.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitSupprimer() {
		if (pSelected != null) this.btn_supprimer.setStyle("-fx-background-color:  #74B8FF");
	}

	public TableView<Periodicite> getTab_periodicite() {
		return tab_periodicite;
	}

	public void setTab_periodicite(TableView<Periodicite> tab_periodicite) {
		this.tab_periodicite = tab_periodicite;
	}
	
	
}
