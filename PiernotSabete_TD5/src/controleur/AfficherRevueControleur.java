package controleur;

import java.io.IOException;

import factory.DAOFactory;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import metier.Revue;
import vue.VueAjouterRevue;
import vue.VueModifierRevue;
import vue.VueRevue;
import vue.VueSupprimerRevue;

public class AfficherRevueControleur implements EventHandler<ActionEvent>, ChangeListener<Revue> {
	@FXML
	private TableView<Revue> tab_revue;
	@FXML
	private Button btn_ajouter;
	@FXML
	private Button btn_modifier;
	@FXML
	private Button btn_supprimer;
	@FXML
	private Label lbl_erreur;
	
	private DAOFactory daos;
	
	private Revue rSelected = null;
	
	private VueRevue vue;
	
	public void initial() {
		//Evenement
		this.tab_revue.getSelectionModel().selectedItemProperty().addListener(this);

		// Cr�ation colonnes
		TableColumn<Revue, String> colTitre = new TableColumn<Revue, String>("Titre");
		TableColumn<Revue, String> colDescription = new TableColumn<Revue, String>("Description");
		TableColumn<Revue, Float> colTarif = new TableColumn<Revue, Float>("Tarif");
		TableColumn<Revue, String> colPeriodicite = new TableColumn<Revue, String>("Periodicite");

		colTitre.setCellValueFactory(new PropertyValueFactory<Revue, String>("titre"));
		colTitre.setStyle("-fx-min-width : 130; -fx-pref-width : 130");
		colDescription.setCellValueFactory(new PropertyValueFactory<Revue, String>("description"));
		colDescription.setStyle("-fx-min-width : 200; -fx-pref-width : 200");
		colTarif.setCellValueFactory(new PropertyValueFactory<Revue, Float>("tarifNumero"));
		colTarif.setStyle("-fx-min-width : 80; -fx-pref-width : 80");
		colPeriodicite.setCellValueFactory(new PropertyValueFactory<Revue, String>("libelle"));
		colPeriodicite.setStyle("-fx-min-width : 120; -fx-pref-width : 120");

		this.tab_revue.getColumns().setAll(colTitre, colDescription, colTarif, colPeriodicite);
		
		this.tab_revue.getItems().addAll(daos.getRevueDAO().findAll());
		
	}

	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}
	
	public void clickAjouter() throws IOException {
		new VueAjouterRevue(daos, this);
	}
	
	public void clickModifier() throws IOException {
		new VueModifierRevue(daos, this, rSelected);
	}
	
	public void clickSupprimer() throws IOException {
		new VueSupprimerRevue(daos, this, rSelected);
	}

	public void setVue(VueRevue vueRevue) {
		this.vue = vueRevue;
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}
	
	@Override
	public void changed(ObservableValue<? extends Revue> observable, Revue oldValue, Revue newValue) {
		// TODO Auto-generated method stub
		this.btn_modifier.setDisable(newValue == null);
		this.btn_supprimer.setDisable(newValue == null);
		
		if (newValue != null) rSelected = newValue;
	}

	
	public void enterAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterModifier() {
		if (rSelected != null) this.btn_modifier.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitModifier() {
		if (rSelected != null) this.btn_modifier.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterSupprimer() {
		if (rSelected != null) this.btn_supprimer.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitSupprimer() {
		if (rSelected != null) this.btn_supprimer.setStyle("-fx-background-color:  #74B8FF");
	}

	public TableView<Revue> getTab_revue() {
		return tab_revue;
	}

	public void setTab_revue(TableView<Revue> tab_revue) {
		this.tab_revue = tab_revue;
	}
}
