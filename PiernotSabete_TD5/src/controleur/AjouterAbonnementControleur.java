package controleur;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import factory.DAOFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import metier.Abonnement;
import metier.Client;
import metier.Date;
import metier.Periodicite;
import metier.Revue;
import vue.VueAjouterAbonnement;

public class AjouterAbonnementControleur implements EventHandler<ActionEvent> {
	@FXML
	private Label lbl_resultat;
	@FXML
	private ComboBox<Client> cb_client;
	@FXML
	private ComboBox<Revue> cb_revue;
	@FXML
	private DatePicker datedeb;
	@FXML
	private DatePicker datefin;
	@FXML
	private Button btn_ajouter;

	private DAOFactory daos = null;

	private VueAjouterAbonnement vue;

	private AfficherAbonnementControleur ctrl;

	private ArrayList<Client> listeCl;
	private ArrayList<Revue> listeRe;

	public void initClient() {
		listeCl = daos.getClientDAO().findAll();
		ObservableList<Client> options = FXCollections.observableArrayList(listeCl);
		cb_client.setItems(options);
	}

	public void initRevue() {
		listeRe = daos.getRevueDAO().findAll();
		ObservableList<Revue> options = FXCollections.observableArrayList(listeRe);
		cb_revue.setItems(options);
	}

	public void clickAjouter() {
		if (this.datedeb.getValue() == null || this.datefin.getValue() == null) {
			this.lbl_resultat.setText("Tous les champs ne sont pas remplis");
		}
		else {
			try {
				int revue = this.cb_revue.getValue().getIdRevue();
				int client = this.cb_client.getValue().getIdcl();
				String dd = this.datedeb.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				int ddj = Integer.parseInt(this.datedeb.getValue().format(DateTimeFormatter.ofPattern("dd")));
				int ddm = Integer.parseInt(this.datedeb.getValue().format(DateTimeFormatter.ofPattern("MM")));
				int dda = Integer.parseInt(this.datedeb.getValue().format(DateTimeFormatter.ofPattern("yyyy")));
				
				String df = this.datefin.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				int dfj = Integer.parseInt(this.datefin.getValue().format(DateTimeFormatter.ofPattern("dd")));
				int dfm = Integer.parseInt(this.datefin.getValue().format(DateTimeFormatter.ofPattern("MM")));
				int dfa = Integer.parseInt(this.datefin.getValue().format(DateTimeFormatter.ofPattern("yyyy")));
				
				daos.getAbonnementDAO().create(new Abonnement(client, revue, new Date(ddj, ddm, dda), new Date(dfj, dfm, dfa)));
				this.lbl_resultat.setText("Abonnement ajout� du " + dd + " au " + df);
				
				actualiserAbonnement();
				
				viderChamps();
			}
			catch (Exception e) {
				this.lbl_resultat.setText("Tous les champs ne sont pas remplis");
			}
		}
	}

	public void actualiserAbonnement() {
		ctrl.getTab_abo().getItems().clear();
		ctrl.getTab_abo().getItems().addAll(daos.getAbonnementDAO().findAll());
	}

	public void viderChamps() {
		cb_client.getSelectionModel().clearSelection();
		cb_revue.getSelectionModel().clearSelection();
		datedeb.setValue(null);
		datefin.setValue(null);

	}
	
	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}
	
	public void enterAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #74B8FF");
	}

	public void setVue(VueAjouterAbonnement vueAjouterAbonnement) {
		this.vue = vueAjouterAbonnement;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public void setCtrl(AfficherAbonnementControleur ctrl) {
		this.ctrl = ctrl;
	}

}
