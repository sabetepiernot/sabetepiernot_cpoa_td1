package controleur;

import java.io.IOException;

import factory.DAOFactory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import metier.Client;
import metier.Revue;
import vue.VueAjouterClient;
import vue.VueClient;

public class AjouterClientControleur implements EventHandler<ActionEvent> {

	@FXML
	private Label lbl_resultat;

	@FXML
	private Button btn_ajouter;

	@FXML
	private TextField edt_nom;
	@FXML
	private TextField edt_prenom;
	@FXML
	private TextField edt_rue;

	@FXML
	private TextField edt_voie;

	@FXML
	private TextField edt_cp;

	@FXML
	private TextField edt_ville;

	@FXML
	private TextField edt_pays;

	private AfficherClientControleur ctrl;

	private DAOFactory daos = null;

	private VueAjouterClient vue;

	@FXML
	public void initialize() {

		this.lbl_resultat.setText("");

	}

	public void ajouterCl() {

		if (this.edt_nom.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le nom");
		} else if (this.edt_prenom.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le pr�nom");
		} else if (this.edt_rue.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le n� de rue");
		} else if (this.edt_voie.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour la voie");
		} else if (this.edt_cp.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le code postal");
		} else if (this.edt_ville.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour la ville");
		} else if (this.edt_pays.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le pays");
		} else {
			String nom = this.edt_nom.getText();
			String prenom = this.edt_prenom.getText();
			String norue = this.edt_rue.getText();
			String voie = this.edt_voie.getText();
			String cp = this.edt_cp.getText();
			String ville = this.edt_ville.getText();
			String pays = this.edt_pays.getText();

			daos.getClientDAO().create(new Client(0, nom, prenom, norue, voie, cp, ville, pays));
			this.lbl_resultat.setText("Client ajout� : " + nom + " " + prenom);

			actualiserClient();

			viderChamps();

		}

	}

	public DAOFactory getDaos() {
		return daos;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public void actualiserClient() {
		ctrl.getTab_client().getItems().clear();
		ctrl.getTab_client().getItems().addAll(daos.getClientDAO().findAll());
	}

	public void viderChamps() {
		edt_nom.setText("");
		edt_prenom.setText("");
		edt_rue.setText("");
		edt_voie.setText("");
		edt_cp.setText("");
		edt_ville.setText("");
		edt_pays.setText("");
	}

	public void enterAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #53A1F4");
	}

	public void exitAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #74B8FF");
	}

	@Override
	public void handle(ActionEvent event) {
		// TODO Auto-generated method stub

	}

	public void setVue(VueAjouterClient vueAjouterClient) {
		// TODO Auto-generated method stub

		this.vue = vueAjouterClient;
	}

	public AfficherClientControleur getCtrl() {
		return ctrl;
	}

	public void setCtrl(AfficherClientControleur ctrl) {
		this.ctrl = ctrl;
	}

	public VueAjouterClient getVue() {
		return vue;
	}

}
