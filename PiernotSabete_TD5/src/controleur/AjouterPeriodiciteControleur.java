package controleur;

import java.util.ArrayList;

import factory.DAOFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import metier.Periodicite;
import vue.VueAjouterPeriodicite;

public class AjouterPeriodiciteControleur implements EventHandler<ActionEvent> {
	@FXML
	private Label lbl_resultat;
	@FXML
	private TextField edt_libelle;
	@FXML
	private Button btn_ajouter;
	
	private AfficherPeriodiciteControleur ctrl;
	
	private DAOFactory daos = null;
	
	private VueAjouterPeriodicite vue;
	
	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void creerModele() {
		if (!(this.edt_libelle.getText().isEmpty())) {
			String libelle = this.edt_libelle.getText();
			try {
				daos.getPeriodiciteDAO().create(new Periodicite(0, libelle));
				this.lbl_resultat.setText(libelle);
				
				actualiserPeriodicite();
				
				viderChamps();
			}
			catch (Exception e) {
				this.lbl_resultat.setText("Echec de l'ajout");
			}
		}
		else {
			this.lbl_resultat.setText("Mauvaise saisie de la périodicité");
		}
	}
	
	public void actualiserPeriodicite() {
		ctrl.getTab_periodicite().getItems().clear();
		ctrl.getTab_periodicite().getItems().addAll(daos.getPeriodiciteDAO().findAll());
	}
	
	public void viderChamps() {
		edt_libelle.setText("");
	}

	public void clickAjouter() {
		creerModele();
	}
	
	public void enterAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAjouter() {
		this.btn_ajouter.setStyle("-fx-background-color:  #74B8FF");
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public VueAjouterPeriodicite getVue() {
		return vue;
	}

	public void setVue(VueAjouterPeriodicite vue) {
		this.vue = vue;
	}

	public AfficherPeriodiciteControleur getCtrl() {
		return ctrl;
	}

	public void setCtrl(AfficherPeriodiciteControleur ctrl) {
		this.ctrl = ctrl;
	}

}
