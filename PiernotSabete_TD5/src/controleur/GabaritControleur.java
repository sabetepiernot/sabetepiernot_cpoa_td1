package controleur;

import java.io.IOException;

import enumerations.EPersistance;
import factory.DAOFactory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.layout.AnchorPane;
import vue.VueAbonnement;
import vue.VueClient;
import vue.VuePeriodicite;
import vue.VueRevue;

public class GabaritControleur implements EventHandler<ActionEvent> {
	@FXML
	private RadioButton rb_mysql;
	@FXML
	private RadioButton rb_lm;
	@FXML
	private Button btn_periodicite;
	@FXML
	private Button btn_client;
	@FXML
	private Button btn_revue;
	@FXML
	private Button btn_abonnement;
	@FXML
	private AnchorPane anchor;
	
	private DAOFactory daos = DAOFactory.getDAOFactory(EPersistance.MYSQL);
	
	@FXML
	public void initialize() {
		
	}
	
	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void clickMySQL() {
		this.rb_mysql.setSelected(true);
		this.rb_lm.setSelected(false);
		
		daos = DAOFactory.getDAOFactory(EPersistance.MYSQL);
	}
	
	public void clickListeMemoire() {
		this.rb_mysql.setSelected(false);
		this.rb_lm.setSelected(true);
		
		daos = DAOFactory.getDAOFactory(EPersistance.MEMORY);
	}

	public void openPeriodicite() throws IOException {
		new VuePeriodicite(daos);
	}

	public void openClient() throws IOException {
		new VueClient(daos);
	}

	public void openRevue() throws IOException {
		new VueRevue(daos);
	}

	public void openAbonnement() throws IOException {
		new VueAbonnement(daos);
	}
	
	
	public void enterPeriodicite() {
		this.btn_periodicite.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitPeriodicite() {
		this.btn_periodicite.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterClient() {
		this.btn_client.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitClient() {
		this.btn_client.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterRevue() {
		this.btn_revue.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitRevue() {
		this.btn_revue.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterAbonnement() {
		this.btn_abonnement.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAbonnement() {
		this.btn_abonnement.setStyle("-fx-background-color:  #74B8FF");
	}
	

}
