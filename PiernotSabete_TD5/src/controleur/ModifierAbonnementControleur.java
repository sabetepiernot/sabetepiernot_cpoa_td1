package controleur;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import factory.DAOFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import metier.Abonnement;
import metier.Client;
import metier.Date;
import metier.Revue;
import vue.VueModifierAbonnement;

public class ModifierAbonnementControleur implements EventHandler<ActionEvent> {
	@FXML
	private Label lbl_resultat;
	@FXML
	private DatePicker datedeb;
	@FXML
	private DatePicker datefin;
	@FXML
	private Button btn_modifier;

	private DAOFactory daos = null;
	private VueModifierAbonnement vue;
	private Abonnement abonnement;
	private AfficherAbonnementControleur ctrl;
	
	public void clickModifier() {
		if (this.datedeb.getValue() == null || this.datefin.getValue() == null) {
			this.lbl_resultat.setText("Tous les champs ne sont pas remplis");
		}
		else {
			try {
				int revue = abonnement.getIdrevue();
				int client = abonnement.getIdclient();
				String dd = this.datedeb.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				int ddj = Integer.parseInt(this.datedeb.getValue().format(DateTimeFormatter.ofPattern("dd")));
				int ddm = Integer.parseInt(this.datedeb.getValue().format(DateTimeFormatter.ofPattern("MM")));
				int dda = Integer.parseInt(this.datedeb.getValue().format(DateTimeFormatter.ofPattern("yyyy")));
				
				String df = this.datefin.getValue().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
				int dfj = Integer.parseInt(this.datefin.getValue().format(DateTimeFormatter.ofPattern("dd")));
				int dfm = Integer.parseInt(this.datefin.getValue().format(DateTimeFormatter.ofPattern("MM")));
				int dfa = Integer.parseInt(this.datefin.getValue().format(DateTimeFormatter.ofPattern("yyyy")));
				
				daos.getAbonnementDAO().update(new Abonnement(client, revue, new Date(ddj, ddm, dda), new Date(dfj, dfm, dfa)));
				this.lbl_resultat.setText("Abonnement modifi� du " + dd + " au " + df);
				
				actualiserAbonnement();
			}
			catch (Exception e) {
				this.lbl_resultat.setText("Tous les champs ne sont pas remplis");
			}
		}
	}
	
	public void initial() {
		this.datedeb.setValue(LocalDate.of(abonnement.getDatedebut().getAnnee(), abonnement.getDatedebut().getMois(), abonnement.getDatedebut().getJour()));
		this.datefin.setValue(LocalDate.of(abonnement.getDatefin().getAnnee(), abonnement.getDatefin().getMois(), abonnement.getDatefin().getJour()));
	}

	public void actualiserAbonnement() {
		ctrl.getTab_abo().getItems().clear();
		ctrl.getTab_abo().getItems().addAll(daos.getAbonnementDAO().findAll());
	}

	@Override
	public void handle(ActionEvent event) {
		// TODO Auto-generated method stub

	}

	public void setVue(VueModifierAbonnement vueModifierAbonnement) {
		// TODO Auto-generated method stub
		this.vue = vueModifierAbonnement;
	}

	public void setAbonnement(Abonnement abo) {
		// TODO Auto-generated method stub
		this.abonnement = abo;
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public VueModifierAbonnement getVue() {
		return vue;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public void setCtrl(AfficherAbonnementControleur ctrl) {
		// TODO Auto-generated method stub
		this.ctrl = ctrl;
	}

	public AfficherAbonnementControleur getCtrl() {
		return ctrl;
	}
	
	public void enterModifier() {
		this.btn_modifier.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitModifier() {
		this.btn_modifier.setStyle("-fx-background-color:  #74B8FF");
	}

}
