package controleur;

import factory.DAOFactory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import metier.Client;
import vue.VueModifierClient;

public class ModifierClientControleur implements EventHandler<ActionEvent> {

	@FXML
	private Label lbl_resultat;

	@FXML
	private Button btn_modifier;

	@FXML
	private TextField edt_nom;
	@FXML
	private TextField edt_prenom;
	@FXML
	private TextField edt_rue;

	@FXML
	private TextField edt_voie;

	@FXML
	private TextField edt_cp;

	@FXML
	private TextField edt_ville;

	@FXML
	private TextField edt_pays;

	private Client client;
	private AfficherClientControleur ctrl;

	private DAOFactory daos = null;

	private VueModifierClient vue;

	public void modifierCl() {

		if (this.edt_nom.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le nom");
		} else if (this.edt_prenom.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le pr�nom");
		} else if (this.edt_rue.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le n� de rue");
		} else if (this.edt_voie.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour la voie");
		} else if (this.edt_cp.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le code postal");
		} else if (this.edt_ville.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour la ville");
		} else if (this.edt_pays.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le pays");
		} else {
			String nom = this.edt_nom.getText();
			String prenom = this.edt_prenom.getText();
			String norue = this.edt_rue.getText();
			String voie = this.edt_voie.getText();
			String cp = this.edt_cp.getText();
			String ville = this.edt_ville.getText();
			String pays = this.edt_pays.getText();

			daos.getClientDAO().update(new Client(client.getIdcl(), nom, prenom, norue, voie, cp, ville, pays));
			this.lbl_resultat.setText("Client modifi� : " + nom + " " + prenom);

			actualiserClient();

		}
	}

	public void actualiserClient() {
		ctrl.getTab_client().getItems().clear();
		ctrl.getTab_client().getItems().addAll(daos.getClientDAO().findAll());

	}

	public void initial() {

		edt_nom.setText(client.getNom());
		edt_prenom.setText(client.getPrenom());
		edt_rue.setText(client.getNorue());
		edt_voie.setText(client.getVoie());
		edt_cp.setText(client.getCodepostal());
		edt_ville.setText(client.getVille());
		edt_pays.setText(client.getPays());

		this.lbl_resultat.setText("");

	}

	public VueModifierClient getVue() {
		return vue;
	}

	public AfficherClientControleur getCtrl() {
		return ctrl;
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public void setCtrl(AfficherClientControleur ctrl) {
		this.ctrl = ctrl;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public void setVue(VueModifierClient vueModifierClient) {
		// TODO Auto-generated method stub

		this.vue = vueModifierClient;
	}

	@Override
	public void handle(ActionEvent event) {
		// TODO Auto-generated method stub

	}

	public void setClient(Client client) {
		// TODO Auto-generated method stub
		this.client = client;
	}
	
	
	public void enterModifier() {
		this.btn_modifier.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitModifier() {
		this.btn_modifier.setStyle("-fx-background-color:  #74B8FF");
	}


}
