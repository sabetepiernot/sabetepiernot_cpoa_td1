package controleur;

import java.util.ArrayList;

import factory.DAOFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import metier.Periodicite;
import metier.Revue;
import vue.VueAjouterPeriodicite;
import vue.VueModifierPeriodicite;
import vue.VueModifierRevue;

public class ModifierPeriodiciteControleur implements EventHandler<ActionEvent> {
	@FXML
	private Label lbl_resultat;
	@FXML
	private TextField edt_libelle;
	@FXML
	private Button btn_modifier;
	
	private AfficherPeriodiciteControleur ctrl;
	
	private DAOFactory daos = null;
	
	private VueModifierPeriodicite vue;
	private Periodicite periodicite;
	
	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void updateModele() {
		if (!(this.edt_libelle.getText().isEmpty())) {
			String libelle = this.edt_libelle.getText();
			try {
				daos.getPeriodiciteDAO().update(new Periodicite(periodicite.getIdPeriodicite(), libelle));
				this.lbl_resultat.setText(libelle);
				
				actualiserPeriodicite();
			}
			catch (Exception e) {
				this.lbl_resultat.setText("Echec de la modification");
			}
		}
		else {
			this.lbl_resultat.setText("Mauvaise saisie de la périodicité");
		}
	}
	
	public void actualiserPeriodicite() {
		ctrl.getTab_periodicite().getItems().clear();
		ctrl.getTab_periodicite().getItems().addAll(daos.getPeriodiciteDAO().findAll());
	}
	
	public void initial() {
		edt_libelle.setText(periodicite.getLibelle());
	}

	public void clickModifier() {
		updateModele();
	}
	
	public void enterModifier() {
		this.btn_modifier.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitModifier() {
		this.btn_modifier.setStyle("-fx-background-color:  #74B8FF");
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public VueModifierPeriodicite getVue() {
		return vue;
	}

	public void setVue(VueModifierPeriodicite vue) {
		this.vue = vue;
	}

	public AfficherPeriodiciteControleur getCtrl() {
		return ctrl;
	}

	public void setCtrl(AfficherPeriodiciteControleur ctrl) {
		this.ctrl = ctrl;
	}

	public Periodicite getPeriodicite() {
		return periodicite;
	}

	public void setPeriodicite(Periodicite periodicite) {
		this.periodicite = periodicite;
	}
	
}
