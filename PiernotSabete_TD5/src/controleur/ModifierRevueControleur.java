package controleur;

import java.util.ArrayList;

import factory.DAOFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import metier.Periodicite;
import metier.Revue;
import vue.VueAjouterRevue;
import vue.VueModifierRevue;

public class ModifierRevueControleur implements EventHandler<ActionEvent> {
	@FXML
	private Label lbl_resultat;
	@FXML
	private ComboBox<Periodicite> cb_periodicite;
	@FXML
	private TextField edt_titre;
	@FXML
	private TextField edt_description;
	@FXML
	private TextField edt_tarif;
	@FXML
	private Button btn_modifier;
	
	private AfficherRevueControleur ctrl;
	
	private DAOFactory daos = null;
	private ArrayList<Periodicite> liste;
	
	private VueModifierRevue vue;
	private Revue revue;
	
	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void updateModele() {
		int bon = 1;
		float tarif = 0;
		try {
			tarif = Float.parseFloat(this.edt_tarif.getText());
		}
		catch (NumberFormatException e) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le tarif");
			bon = 0;
		}
		if (this.edt_titre.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le titre");
		}
		else if (this.edt_tarif.getText().isEmpty()) {
			this.lbl_resultat.setText("Mauvaise entr�e pour le tarif");
		}
		else {
			if (bon == 1) {
				String titre = this.edt_titre.getText();
				String desc = this.edt_description.getText();
				try {
					int idPerio = this.cb_periodicite.getValue().getIdPeriodicite();
					daos.getRevueDAO().update(new Revue(revue.getIdRevue(),titre,desc,(float) tarif,revue.getVisuel(),idPerio));
					this.lbl_resultat.setText(titre+" ("+tarif+" euros)");
					
					actualiserRevue();
				}
				catch (Exception e) {
					this.lbl_resultat.setText("Veuillez selectionner un �l�ment pour la p�riodicit�");
				}
				
			}
		}
	}
	
	public void actualiserRevue() {
		ctrl.getTab_revue().getItems().clear();
		ctrl.getTab_revue().getItems().addAll(daos.getRevueDAO().findAll());
	}
	
	public void initPeriodicite() {
		liste = daos.getPeriodiciteDAO().findAll();
		ObservableList<Periodicite> options = FXCollections.observableArrayList(liste);
		cb_periodicite.setItems(options);
	}
	
	public void initial() {
		edt_titre.setText(revue.getTitre());
		edt_description.setText(revue.getDescription());
		edt_tarif.setText(String.valueOf(revue.getTarifNumero()));
		cb_periodicite.setValue(new Periodicite(revue.getIdPeriodicite(), revue.getLibelle()));
	}

	public void clickModifier() {
		updateModele();
	}
	
	public void enterModifier() {
		this.btn_modifier.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitModifier() {
		this.btn_modifier.setStyle("-fx-background-color:  #74B8FF");
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public VueModifierRevue getVue() {
		return vue;
	}

	public void setVue(VueModifierRevue vue) {
		this.vue = vue;
	}

	public AfficherRevueControleur getCtrl() {
		return ctrl;
	}

	public void setCtrl(AfficherRevueControleur ctrl) {
		this.ctrl = ctrl;
	}

	public Revue getRevue() {
		return revue;
	}

	public void setRevue(Revue revue) {
		this.revue = revue;
	}
	
}
