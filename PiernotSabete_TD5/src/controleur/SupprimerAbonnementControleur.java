package controleur;

import factory.DAOFactory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import metier.Abonnement;
import vue.VueSupprimerAbonnement;

public class SupprimerAbonnementControleur implements EventHandler<ActionEvent> {
	@FXML
	private Label lbl_description;
	@FXML
	private Button btn_supprimer;
	@FXML
	private Button btn_annuler;

	private AfficherAbonnementControleur ctrl;

	private DAOFactory daos = null;

	private VueSupprimerAbonnement vue;

	private Abonnement abonnement;

	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

	public void initial() {
		lbl_description.setText(lbl_description.getText() + abonnement.getIdclient() + " " + abonnement.getIdrevue() + " du " + abonnement.getDd() + " au " + abonnement.getDf() + " ?");
	}

	public void clickAnnuler() {
		vue.close();
	}

	public void clickSupprimer() {

		daos.getAbonnementDAO().delete(abonnement);

		actualiserAbonnement();

		vue.close();
	}

	public void actualiserAbonnement() {
		ctrl.getTab_abo().getItems().clear();
		ctrl.getTab_abo().getItems().addAll(daos.getAbonnementDAO().findAll());
	}

	public AfficherAbonnementControleur getCtrl() {
		return ctrl;
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public VueSupprimerAbonnement getVue() {
		return vue;
	}

	public Abonnement getAbonnement() {
		return abonnement;
	}

	public void setCtrl(AfficherAbonnementControleur ctrl) {
		this.ctrl = ctrl;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public void setVue(VueSupprimerAbonnement vue) {
		this.vue = vue;
	}

	public void setAbonnement(Abonnement abonnement) {
		this.abonnement = abonnement;
	}
	
	public void enterSupprimer() {
		this.btn_supprimer.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitSupprimer() {
		this.btn_supprimer.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterAnnuler() {
		this.btn_annuler.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAnnuler() {
		this.btn_annuler.setStyle("-fx-background-color:  #74B8FF");
	}

}
