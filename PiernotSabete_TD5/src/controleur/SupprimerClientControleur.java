package controleur;

import factory.DAOFactory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import metier.Client;
import vue.VueSupprimerClient;

public class SupprimerClientControleur implements EventHandler<ActionEvent> {

	@FXML
	private Label lbl_description;
	@FXML
	private Button btn_supprimer;
	@FXML
	private Button btn_annuler;

	private AfficherClientControleur ctrl;

	private DAOFactory daos = null;

	private VueSupprimerClient vue;

	private Client client;

	public void initial() {
		// TODO Auto-generated method stub

		lbl_description.setText(lbl_description.getText() + client.getNom() + " " + client.getPrenom() + "\" ?");

	}

	public void supprimerCl() {
		daos.getClientDAO().delete(client);

		actualiserClient();
		vue.close();
	}

	public void actualiserClient() {
		ctrl.getTab_client().getItems().clear();
		ctrl.getTab_client().getItems().addAll(daos.getClientDAO().findAll());
	}

	public void annuler() {
		vue.close();
	}
	
	public void enterSupprimer() {
		this.btn_supprimer.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitSupprimer() {
		this.btn_supprimer.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterAnnuler() {
		this.btn_annuler.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAnnuler() {
		this.btn_annuler.setStyle("-fx-background-color:  #74B8FF");
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public AfficherClientControleur getCtrl() {
		return ctrl;
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public VueSupprimerClient getVue() {
		return vue;
	}

	public void setCtrl(AfficherClientControleur ctrl) {
		this.ctrl = ctrl;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public void setVue(VueSupprimerClient vue) {
		this.vue = vue;
	}

	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

}
