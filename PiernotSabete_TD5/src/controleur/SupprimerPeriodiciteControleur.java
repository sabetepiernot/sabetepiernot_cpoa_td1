package controleur;

import factory.DAOFactory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import metier.Periodicite;
import vue.VueSupprimerPeriodicite;

public class SupprimerPeriodiciteControleur implements EventHandler<ActionEvent> {
	@FXML
	private Label lbl_description;
	@FXML
	private Button btn_supprimer;
	@FXML
	private Button btn_annuler;
	
	private AfficherPeriodiciteControleur ctrl;
	
	private DAOFactory daos = null;
	
	private VueSupprimerPeriodicite vue;
	
	private Periodicite periodicite;
	
	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void deleteModele() {
		daos.getPeriodiciteDAO().delete(periodicite);
		
		actualiserPeriodicite();
	}
	
	public void actualiserPeriodicite() {
		ctrl.getTab_periodicite().getItems().clear();
		ctrl.getTab_periodicite().getItems().addAll(daos.getPeriodiciteDAO().findAll());
	}
	
	public void initial() {
		lbl_description.setText(lbl_description.getText() + periodicite.getLibelle() + "\" ?");
	}

	public void clickSupprimer() {
		deleteModele();
		
		vue.close();
	}

	public void clickAnnuler() {
		vue.close();
	}
	
	public void enterSupprimer() {
		this.btn_supprimer.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitSupprimer() {
		this.btn_supprimer.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterAnnuler() {
		this.btn_annuler.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAnnuler() {
		this.btn_annuler.setStyle("-fx-background-color:  #74B8FF");
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public VueSupprimerPeriodicite getVue() {
		return vue;
	}

	public void setVue(VueSupprimerPeriodicite vue) {
		this.vue = vue;
	}

	public AfficherPeriodiciteControleur getCtrl() {
		return ctrl;
	}

	public void setCtrl(AfficherPeriodiciteControleur ctrl) {
		this.ctrl = ctrl;
	}

	public Periodicite getPeriodicite() {
		return periodicite;
	}

	public void setPeriodicite(Periodicite revue) {
		this.periodicite = revue;
	}
	
}
