package controleur;

import factory.DAOFactory;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import metier.Periodicite;
import metier.Revue;
import vue.VueModifierRevue;
import vue.VueSupprimerRevue;

public class SupprimerRevueControleur implements EventHandler<ActionEvent> {
	@FXML
	private Label lbl_description;
	@FXML
	private Button btn_supprimer;
	@FXML
	private Button btn_annuler;
	
	private AfficherRevueControleur ctrl;
	
	private DAOFactory daos = null;
	
	private VueSupprimerRevue vue;
	
	private Revue revue;
	
	@Override
	public void handle(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	public void deleteModele() {
		daos.getRevueDAO().delete(revue);
		
		actualiserRevue();
		
		vue.close();
	}
	
	public void actualiserRevue() {
		ctrl.getTab_revue().getItems().clear();
		ctrl.getTab_revue().getItems().addAll(daos.getRevueDAO().findAll());
	}
	
	public void initial() {
		lbl_description.setText(lbl_description.getText() + revue.getTitre() + "\" ?");
	}

	public void clickSupprimer() {
		deleteModele();
		
		vue.close();
	}

	public void clickAnnuler() {
		vue.close();
	}
	
	public void enterSupprimer() {
		this.btn_supprimer.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitSupprimer() {
		this.btn_supprimer.setStyle("-fx-background-color:  #74B8FF");
	}
	
	public void enterAnnuler() {
		this.btn_annuler.setStyle("-fx-background-color:  #53A1F4");
	}
	
	public void exitAnnuler() {
		this.btn_annuler.setStyle("-fx-background-color:  #74B8FF");
	}

	public DAOFactory getDaos() {
		return daos;
	}

	public void setDaos(DAOFactory daos) {
		this.daos = daos;
	}

	public VueSupprimerRevue getVue() {
		return vue;
	}

	public void setVue(VueSupprimerRevue vue) {
		this.vue = vue;
	}

	public AfficherRevueControleur getCtrl() {
		return ctrl;
	}

	public void setCtrl(AfficherRevueControleur ctrl) {
		this.ctrl = ctrl;
	}

	public Revue getRevue() {
		return revue;
	}

	public void setRevue(Revue revue) {
		this.revue = revue;
	}
	
}
