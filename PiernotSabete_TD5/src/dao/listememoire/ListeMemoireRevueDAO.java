package dao.listememoire;

import java.util.ArrayList;
import java.util.List;

import enumerations.EPersistance;
import factory.DAOFactory;
import interfaces.IRevueDAO;
import metier.Client;
import metier.Periodicite;
import metier.Revue;

public class ListeMemoireRevueDAO implements IRevueDAO {

	private static ListeMemoireRevueDAO instance;

	private List<Revue> donnees;

	private ListeMemoireRevueDAO() {
		this.donnees = new ArrayList<Revue>();
		
		this.donnees.add(new Revue(1, "Paris Match num�ro 256", "Journal � scandale", (float) 3.50, "Photo Johnny avec sa femme", 1));
		this.donnees.add(new Revue(2, "Paris Match num�ro 257", "Journal � scandale", (float) 3.50, "Emmanuel Macron", 1));
		this.donnees.add(new Revue(3, "Le monde des chats", "Apprendre � mieux conna�tre les chats", (float) 5.50, "Chats", 2));
		this.donnees.add(new Revue(4, "Le monde des sciences", "Revue scientifique", (float) 5.00, "Atome sch�matis�", 2));
	}

	public static ListeMemoireRevueDAO getInstance() {
		if (instance == null) {
			instance = new ListeMemoireRevueDAO();
		}
		return instance;
	}

	@Override
	public boolean create(Revue r) {
		try {
			int max = 1;
			for (int i = 0; i < this.donnees.size(); i++) {
				if (this.donnees.get(i).getIdRevue() >= max) {
					max = this.donnees.get(i).getIdRevue() + 1;
				}
			}
			r.setIdRevue(max);
			this.donnees.add(r);
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Revue r) {
		try {
			for (int i=0;i<this.donnees.size();i++) {
				if (r.getIdRevue() == this.donnees.get(i).getIdRevue()) {
					this.donnees.remove(i);
				}
			}
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean update(Revue r) {
		try {
			for (int i=0;i<this.donnees.size();i++) {
				if (r.getIdRevue() == this.donnees.get(i).getIdRevue()) {
					this.donnees.get(i).setTitre(r.getTitre());
					this.donnees.get(i).setDescription(r.getDescription());
					this.donnees.get(i).setTarifNumero(r.getTarifNumero());
					this.donnees.get(i).setVisuel(r.getVisuel());
					this.donnees.get(i).setIdPeriodicite(r.getIdPeriodicite());
				}
			}
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean show() {
		try {
			for (int i=0;i<this.donnees.size();i++) {
				int idRevue = this.donnees.get(i).getIdRevue();
				String titre = this.donnees.get(i).getTitre();
				String desc = this.donnees.get(i).getDescription();
				float tarif = this.donnees.get(i).getTarifNumero();
				String visuel = this.donnees.get(i).getVisuel();
				int idPeriodicite = this.donnees.get(i).getIdPeriodicite();
				
				System.out.println(idRevue + " " + titre + " " + desc + " " + tarif + " " + visuel + " " + idPeriodicite);
			}
		}
		catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public Revue getById(int id) {
		try {
			Revue r = new Revue(id, null, null, (float) 0, null, 0);
			for (int i=0;i<this.donnees.size();i++) {
				if (this.donnees.get(i).idRevueEquals(r)) {
					return this.donnees.get(i);
				}
			}
		}
		catch (Exception e) {
			return null;
		}
		return null;
	}

	@Override
	public List<Revue> getByTitre(String titre) {
		List<Revue> res = new ArrayList<Revue>();
		try {
			Revue r = new Revue(0, titre, null, (float) 0, null, 0);
			for (int i=0;i<this.donnees.size();i++) {
				if (this.donnees.get(i).titreEquals(r)) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Revue> getByDescription(String description) {
		List<Revue> res = new ArrayList<Revue>();
		try {
			Revue r = new Revue(0, null, description, (float) 0, null, 0);
			for (int i=0;i<this.donnees.size();i++) {
				if (this.donnees.get(i).descriptionEquals(r)) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Revue> getByTarifNumero(float tarif) {
		List<Revue> res = new ArrayList<Revue>();
		try {
			Revue r = new Revue(0, null, null, tarif, null, 0);
			for (int i=0;i<this.donnees.size();i++) {
				if (this.donnees.get(i).tarifNumeroEquals(r)) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Revue> getByVisuel(String visuel) {
		List<Revue> res = new ArrayList<Revue>();
		try {
			Revue r = new Revue(0, null, null, (float) 0, visuel, 0);
			for (int i=0;i<this.donnees.size();i++) {
				if (this.donnees.get(i).visuelEquals(r)) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Revue> getByIdPeriodicite(int idPeriodicite) {
		List<Revue> res = new ArrayList<Revue>();
		try {
			Revue r = new Revue(0, null, null, (float) 0, null, idPeriodicite);
			for (int i=0;i<this.donnees.size();i++) {
				if (this.donnees.get(i).idPeriodiciteEquals(r)) {
					res.add(this.donnees.get(i));
				}
			}
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

	@Override
	public ArrayList<Revue> findAll() {
		ArrayList<Revue> res = new ArrayList<Revue>();
		try {
			for (int i=0;i<this.donnees.size();i++) {
				Periodicite p = DAOFactory.getDAOFactory(EPersistance.MEMORY).getPeriodiciteDAO().getById(this.donnees.get(i).getIdPeriodicite());
				this.donnees.get(i).setLibelle(p.getLibelle());
				res.add(this.donnees.get(i));
			}
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}

}
