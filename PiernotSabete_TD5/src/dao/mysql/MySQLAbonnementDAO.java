package dao.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import interfaces.IAbonnementDAO;
import methode.Connexion;
import metier.Abonnement;
import metier.Date;

public class MySQLAbonnementDAO implements IAbonnementDAO {

	private static MySQLAbonnementDAO instance;

	private MySQLAbonnementDAO() {

	}

	public static MySQLAbonnementDAO getInstance() {
		if (instance == null) {
			instance = new MySQLAbonnementDAO();

		}
		return instance;
	}

	public Abonnement getById(int idcl, int idrevue) {
		Abonnement abo = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			PreparedStatement requete = laConnexion.prepareStatement("select id_client, id_revue, YEAR(date_debut) dda, MONTH(date_debut) ddm, DAY(date_debut) ddj, YEAR(date_fin) dfa, MONTH(date_fin) dfm, DAY(date_fin) dfj from Abonnement where id_client =? and id_revue =?;");
			requete.setInt(1, idcl);
			requete.setInt(2, idrevue);
			ResultSet res = requete.executeQuery();
			while (res.next()) {
				abo.setIdclient(res.getInt(1));
				abo.setIdrevue(res.getInt(2));
				Date dd = new Date(res.getInt("ddj"), res.getInt("ddm"), res.getInt("dda"));
				Date df = new Date(res.getInt("dfj"), res.getInt("dfm"), res.getInt("dfa"));
				abo.setDatedebut(dd);
				abo.setDatefin(df);

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return abo;
	}

	@Override
	public boolean create(Abonnement abo) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete
					.executeUpdate("INSERT INTO Abonnement (id_client, id_revue, date_debut, date_fin) VALUES ('"
							+ abo.getIdclient() + "','" + abo.getIdrevue() + "',str_to_date('" + abo.getDatedebut()
							+ "', '%d/%m/%Y'),str_to_date('" + abo.getDatefin() + "', '%d/%m/%Y'));");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean update(Abonnement abo) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("UPDATE Abonnement SET date_debut = str_to_date('" + abo.getDatedebut()
					+ "','%d/%m/%Y'), date_fin = str_to_date('" + abo.getDatefin() + "','%d/%m/%Y') WHERE id_client = '"
					+ abo.getIdclient() + "' AND id_revue = '" + abo.getIdrevue() + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Abonnement abo) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("DELETE FROM Abonnement WHERE id_client = '" + abo.getIdclient()
					+ "' AND id_revue = '" + abo.getIdrevue() + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean show() {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet res = requete.executeQuery("SELECT * FROM Abonnement ORDER BY id_client, id_revue;");

			while (res.next()) {
				int no = res.getInt(1);
				int no2 = res.getInt(2);
				String date_debut = res.getString("date_debut");
				String date_fin = res.getString("date_fin");
				System.out.println(no + " " + no2 + " " + date_debut + " " + date_fin);
			}

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public Abonnement getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Abonnement> getAbonnementByIdClient(int idClient) {
		// TODO Auto-generated method stub
		List<Abonnement> resabo = new ArrayList<Abonnement>();
		Abonnement abo = null;

		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery(
					"select id_client, id_revue, YEAR(date_debut) dda, MONTH(date_debut) ddm, DAY(date_debut) ddj, YEAR(date_fin) dfa, MONTH(date_fin) dfm, DAY(date_fin) dfj from Abonnement where id_client = '" + idClient + "' ORDER BY id_client, id_revue ;");

			while (resReq.next()) {
				int idcl = (resReq.getInt(1));
				int idrevue = (resReq.getInt(2));
				Date dd = new Date(resReq.getInt("ddj"), resReq.getInt("ddm"), resReq.getInt("dda"));
				Date df = new Date(resReq.getInt("dfj"), resReq.getInt("dfm"), resReq.getInt("dfa"));
				resabo.add(new Abonnement(idcl, idrevue, dd, df));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return resabo;
	}

	@Override
	public List<Abonnement> getAbonnementByIdRevue(int idRevue) {
		// TODO Auto-generated method stub
		List<Abonnement> resabo = new ArrayList<Abonnement>();
		Abonnement abo = null;

		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery(
					"select id_client, id_revue, YEAR(date_debut) dda, MONTH(date_debut) ddm, DAY(date_debut) ddj, YEAR(date_fin) dfa, MONTH(date_fin) dfm, DAY(date_fin) dfj from Abonnement where id_revue = '" + idRevue + "' ORDER BY id_client, id_revue;");

			while (resReq.next()) {
				int idcl = (resReq.getInt(1));
				int idrevue = (resReq.getInt(2));
				Date dd = new Date(resReq.getInt("ddj"), resReq.getInt("ddm"), resReq.getInt("dda"));
				Date df = new Date(resReq.getInt("dfj"), resReq.getInt("dfm"), resReq.getInt("dfa"));
				resabo.add(new Abonnement(idcl, idrevue, dd, df));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return resabo;
	}

	@Override
	public ArrayList<Abonnement> findAll() {
		ArrayList<Abonnement> res = new ArrayList<Abonnement>();
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery(
					"SELECT id_client, id_revue, YEAR(date_debut) dda, MONTH(date_debut) ddm, DAY(date_debut) ddj, YEAR(date_fin) dfa, MONTH(date_fin) dfm, DAY(date_fin) dfj FROM Abonnement ORDER BY id_client, id_revue;");
			while (resReq.next()) {
				int idcl = resReq.getInt(1);
				int idrevue = resReq.getInt(2);
				Date dd = new Date(resReq.getInt("ddj"), resReq.getInt("ddm"), resReq.getInt("dda"));
				Date df = new Date(resReq.getInt("dfj"), resReq.getInt("dfm"), resReq.getInt("dfa"));
				String strDd = resReq.getInt("ddj") + "/" + resReq.getInt("ddm") + "/" + resReq.getInt("dda");
				String strDf = resReq.getInt("dfj") + "/" + resReq.getInt("dfm") + "/" + resReq.getInt("dfa");
				Abonnement a = new Abonnement(idcl, idrevue, dd, df);
				a.setDd(strDd);
				a.setDf(strDf);

				res.add(a);
			}
			return res;
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<Abonnement> getAbonnementByDateDebut(Date dateDebut) {
		List<Abonnement> resabo = new ArrayList<Abonnement>();
		Abonnement abo = null;

		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery(
					"select id_client, id_revue, YEAR(date_debut) dda, MONTH(date_debut) ddm, DAY(date_debut) ddj, YEAR(date_fin) dfa, MONTH(date_fin) dfm, DAY(date_fin) dfj from Abonnement where date_debut = '" + dateDebut + "' ORDER BY id_client, id_revue;");

			while (resReq.next()) {
				int idcl = (resReq.getInt(1));
				int idrevue = (resReq.getInt(2));
				Date dd = new Date(resReq.getInt("ddj"), resReq.getInt("ddm"), resReq.getInt("dda"));
				Date df = new Date(resReq.getInt("dfj"), resReq.getInt("dfm"), resReq.getInt("dfa"));
				resabo.add(new Abonnement(idcl, idrevue, dd, df));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return resabo;
	}

	@Override
	public List<Abonnement> getAbonnementByDateFin(Date dateFin) {
		List<Abonnement> resabo = new ArrayList<Abonnement>();
		Abonnement abo = null;

		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery(
					"select id_client, id_revue, YEAR(date_debut) dda, MONTH(date_debut) ddm, DAY(date_debut) ddj, YEAR(date_fin) dfa, MONTH(date_fin) dfm, DAY(date_fin) dfj from Abonnement where date_fin = '" + dateFin + "' ORDER BY id_client, id_revue;");
			while (resReq.next()) {
				int idcl = (resReq.getInt(1));
				int idrevue = (resReq.getInt(2));
				Date dd = new Date(resReq.getInt("ddj"), resReq.getInt("ddm"), resReq.getInt("dda"));
				Date df = new Date(resReq.getInt("dfj"), resReq.getInt("dfm"), resReq.getInt("dfa"));
				resabo.add(new Abonnement(idcl, idrevue, dd, df));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return resabo;
	}

}
