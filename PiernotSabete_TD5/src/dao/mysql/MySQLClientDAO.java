package dao.mysql;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import interfaces.IClientDAO;
import methode.Connexion;
import metier.Client;
import metier.Periodicite;
import normalisation.CodePostal;
import normalisation.NoRue;
import normalisation.Pays;
import normalisation.Ville;
import normalisation.Voie;

public class MySQLClientDAO implements IClientDAO {

	private static MySQLClientDAO instance;

	private MySQLClientDAO() {

	}

	public static MySQLClientDAO getInstance() {
		if (instance == null) {
			instance = new MySQLClientDAO();

		}
		return instance;
	}

	public Client getById(int id) {
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			PreparedStatement requete = laConnexion.prepareStatement("select * from Client where id_client =?;");
			requete.setInt(1, id);
			ResultSet res = requete.executeQuery();
			while (res.next()) {
				cl.setIdcl(res.getInt(1));
				cl.setNom(res.getString("nom"));
				cl.setPrenom(res.getString("prenom"));
				cl.setNorue(res.getString("no_rue"));
				cl.setVoie(res.getString("voie"));
				cl.setCodepostal(res.getString("code_postal"));
				cl.setVille(res.getString("ville"));
				cl.setPays(res.getString("pays"));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return cl;
	}

	@Override
	public boolean create(Client cl) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete
					.executeUpdate("INSERT INTO Client (nom, prenom, no_rue, voie, code_postal, ville, pays) VALUES ('"
							+ cl.getNom() + "','" + cl.getPrenom() + "','" + Voie.voieNormalisation(cl.getNorue()) + "','" + NoRue.nomRueNormalisation(cl.getVoie())
							+ "','" + CodePostal.codepostalNormalisation(cl.getCodepostal()) + "','" + Ville.villeNormalisation(cl.getVille()) + "','" + Pays.paysNormalisation(cl.getPays()) + "');");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean delete(Client cl) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("DELETE FROM Client WHERE id_client = '" + cl.getIdcl() + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}

		return true;
	}

	@Override
	public boolean update(Client cl) {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			int res = requete.executeUpdate("UPDATE Client SET nom = '" + cl.getNom() + "', prenom = '" + cl.getPrenom()
					+ "', no_rue = '" + Voie.voieNormalisation(cl.getNorue()) + "', voie = '" + NoRue.nomRueNormalisation(cl.getVoie()) + "', code_postal = '"
					+ CodePostal.codepostalNormalisation(cl.getCodepostal()) + "', ville = '" + Ville.villeNormalisation(cl.getVille()) + "', pays = '" + Pays.paysNormalisation(cl.getPays())
					+ "' WHERE id_client = '" + cl.getIdcl() + "';");

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;
	}

	@Override
	public boolean show() {
		// TODO Auto-generated method stub
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet res = requete.executeQuery(
					"SELECT id_client, nom, prenom, no_rue, voie, code_postal, ville, pays FROM Client ORDER BY id_client;");

			while (res.next()) {
				int no = res.getInt(1);
				String nom = res.getString("nom");
				String prenom = res.getString("prenom");
				String no_rue = res.getString("no_rue");
				String voie = res.getString("voie");
				String code_postal = res.getString("code_postal");
				String ville = res.getString("ville");
				String pays = res.getString("pays");
				System.out.println(no + " " + nom + " " + prenom + " " + no_rue + " " + voie + " " + code_postal + " "
						+ ville + " " + pays);
			}

		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
			return false;
		}
		return true;

	}

	@Override
	public List<Client> getClientByNom(String nom) {
		// TODO Auto-generated method stub
		
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where nom = '" + nom + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, voie, cp, ville, pays));

				
			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByPrenom(String prenom) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where prenom = '" + prenom + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String Prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, Prenom, norue, voie, cp, ville, pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByNoRue(String noRue) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where no_rue = '" + noRue + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, voie, cp, ville, pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByVoie(String voie) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where voie = '" + voie + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String Voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, Voie, cp, ville, pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByCodePostal(String cp) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where code_postal = '" + cp + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String codepostal=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, voie, codepostal, ville, pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByVille(String ville) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where ville = '" + ville + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String Ville=(resReq.getString("ville"));
				String pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, voie, cp, Ville, pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}

	@Override
	public List<Client> getClientByPays(String pays) {
		// TODO Auto-generated method stub
		List<Client> rescl = new ArrayList<Client>();
		Client cl = null;
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("select * from Client where pays = '" + pays + "' ORDER BY id_client;");
			while (resReq.next()) {
				int idcl=(resReq.getInt(1));
				String Nom=(resReq.getString("nom"));
				String prenom=(resReq.getString("prenom"));
				String norue=(resReq.getString("no_rue"));
				String voie=(resReq.getString("voie"));
				String cp=(resReq.getString("code_postal"));
				String ville=(resReq.getString("ville"));
				String Pays=(resReq.getString("pays"));
				rescl.add(new Client(idcl, Nom, prenom, norue, voie, cp, ville, Pays));

			}
		} catch (SQLException sqle) {
			System.out.println("Pb select : " + sqle.getMessage());
		}

		return rescl;

	}
	
	public void setDataByCSV(String fileName) throws IOException {
		try
		{
		   String chemin = "ressource/" + fileName + ".csv";
		   BufferedReader fichier_source = new BufferedReader(new FileReader(chemin));
		   String chaine;
		   int i = 1;
		 
		   while((chaine = fichier_source.readLine())!= null)
		   {
			   String[] tabChaine = chaine.split(";");
			   Client c = tabToClient(tabChaine);
			   create(c);
		   }
		   fichier_source.close();                 
		}
		catch (FileNotFoundException e)
		{
		   System.out.println("Le fichier est introuvable !");
		}
	}
	
	private Client tabToClient(String[] tab) {
		Client c = new Client();
		
		c.setIdcl(Integer.parseInt(tab[0]));
		c.setNom(tab[1]);
		c.setPrenom(tab[2]);
		c.setNorue(Voie.voieNormalisation(tab[3]));
		c.setVoie(NoRue.nomRueNormalisation(tab[4]));
		c.setCodepostal(CodePostal.codepostalNormalisation(tab[5]));
		c.setVille(Ville.villeNormalisation(tab[6]));
		c.setPays(Pays.paysNormalisation(tab[7]));
		
		return c;
	}

	@Override
	public ArrayList<Client> findAll() {
		ArrayList<Client> res = new ArrayList<Client>();
		try {
			Connection laConnexion = Connexion.getInstance();
			Statement requete = laConnexion.createStatement();
			ResultSet resReq = requete.executeQuery("SELECT * FROM Client;");
			while (resReq.next()) {
				int id = resReq.getInt(1);
				String nom = resReq.getString(2);
				String prenom = resReq.getString(3);
				String norue = resReq.getString(4);
				String voie = resReq.getString(5);
				String cp = resReq.getString(6);
				String ville = resReq.getString(7);
				String pays = resReq.getString(8);
				res.add(new Client(id, nom, prenom, norue, voie, cp, ville, pays));
			}
			return res;
		}
		catch (Exception e) {
			return null;
		}
	}
}
