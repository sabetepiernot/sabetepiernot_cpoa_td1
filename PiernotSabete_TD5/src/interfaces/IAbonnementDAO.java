package interfaces;

import java.util.ArrayList;
import java.util.List;

import metier.Abonnement;
import metier.Date;

public interface IAbonnementDAO extends IDAO<Abonnement> {

	List<Abonnement> getAbonnementByIdClient(int idClient);
	List<Abonnement> getAbonnementByIdRevue(int idRevue);
	List<Abonnement> getAbonnementByDateDebut(Date dateDebut);
	List<Abonnement> getAbonnementByDateFin(Date dateFin);
	ArrayList<Abonnement> findAll();
}
