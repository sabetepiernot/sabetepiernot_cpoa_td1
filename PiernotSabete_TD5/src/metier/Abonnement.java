package metier;

public class Abonnement {
	private int idclient;
	private int idrevue;
	private Date datedebut;
	private Date datefin;
	private String dd;
	private String df;

	public Abonnement(int idcl, int idrevue, Date datedeb, Date datefin) {
		this.idclient = idcl;
		this.idrevue = idrevue;
		this.datedebut = datedeb;
		this.datefin = datefin;
	}

	public Abonnement() {
	}

	public int getIdclient() {
		return idclient;
	}

	public int getIdrevue() {
		return idrevue;
	}

	public Date getDatedebut() {
		return datedebut;
	}

	public Date getDatefin() {
		return datefin;
	}

	public void setIdclient(int idclient) {
		this.idclient = idclient;
	}

	public void setIdrevue(int idrevue) {
		this.idrevue = idrevue;
	}

	public String getDd() {
		return dd;
	}

	public void setDd(String dd) {
		this.dd = dd;
	}

	public String getDf() {
		return df;
	}

	public void setDf(String df) {
		this.df = df;
	}

	public void setDatedebut(Date datedebut) {
		this.datedebut = datedebut;
	}

	public void setDatefin(Date datefin) {
		this.datefin = datefin;
	}
	
	public boolean idClientEquals(Abonnement a) {
		return (this.idclient == a.idclient);
	}
	
	public boolean idRevueEquals(Abonnement a) {
		return (this.idrevue == a.idrevue);
	}
	
	public boolean dateDebutEquals(Abonnement a) {
		return (this.datedebut.equals(a.datedebut));
	}
	
	public boolean dateFinEquals(Abonnement a) {
		return (this.datefin.equals(a.datefin));
	}
}
