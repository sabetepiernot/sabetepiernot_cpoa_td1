package metier;

public class Client {
	private int idcl;
	private String nom;
	private String prenom;
	private String norue;
	private String voie;
	private String codepostal;
	private String ville;
	private String pays;

	public Client(int no, String nom, String prenom, String norue, String voie, String cp, String ville, String pays) {
		this.idcl = no;
		this.nom = nom;
		this.prenom = prenom;
		this.norue = norue;
		this.voie = voie;
		this.codepostal = cp;
		this.ville = ville;
		this.pays = pays;
	}

	public Client() {
	}
	
	

	public int getIdcl() {
		return idcl;
	}

	public String getNom() {
		return nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public String getNorue() {
		return norue;
	}

	public String getVoie() {
		return voie;
	}

	public String getCodepostal() {
		return codepostal;
	}

	public String getVille() {
		return ville;
	}

	public String getPays() {
		return pays;
	}

	public void setIdcl(int idcl) {
		this.idcl = idcl;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public void setNorue(String norue) {
		this.norue = norue;
	}

	public void setVoie(String voie) {
		this.voie = voie;
	}

	public void setCodepostal(String codepostal) {
		this.codepostal = codepostal;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public void setPays(String pays) {
		this.pays = pays;
	}
	
	public boolean idClientEquals(Client c) {
		return (this.idcl == c.idcl);
	}
	
	public boolean nomEquals(Client c) {
		return (this.nom.equals(c.nom));
	}
	
	public boolean prenomEquals(Client c) {
		return (this.prenom.equals(c.prenom));
	}
	
	public boolean noRueEquals(Client c) {
		return (this.norue.equals(c.norue));
	}
	
	public boolean voieEquals(Client c) {
		return (this.voie.equals(c.voie));
	}
	
	public boolean codePostalEquals(Client c) {
		return (this.codepostal.equals(c.codepostal));
	}
	
	public boolean villeEquals(Client c) {
		return (this.ville.equals(c.ville));
	}
	
	public boolean paysEquals(Client c) {
		return (this.pays.equals(c.pays));
	}

	@Override
	public String toString() {
		return nom + " " + prenom + "; " + voie + "; " + ville;
	}

}