package normalisation;

public class Pays extends MethodesCommunes {

	private String pays;

	public Pays(String p) {

		this.pays = this.paysNormalisation(p);

	}

	public static String paysNormalisation(String p) {
		p = removeSpaceBeginEnd(p);
		p = removeSpaceInside(p);
		p = Minuscule(p);
		p = Majuscule(p);
		p = Franciser(p);
		return p;
	}

	// Minuscule

	public static String Minuscule(String p) {

		String chaineMin = p.substring(0, 1) + p.substring(1).toLowerCase();
		return chaineMin;

	}

	// Majuscule

	public static String Majuscule(String p) {

		String chaineMaj = p.replaceFirst(".", (p.charAt(0) + "").toUpperCase());
		return chaineMaj;

	}

	// Franciser
	public static String Franciser(String p)

	{

		switch (p) {
		case "letzebuerg":
			p = "Luxembourg";
			return p;

		case "belgium":
			p = "Belgique";
			return p;

		case "Switzerland":
			p = "Suisse";
			return p;

		case "Schweiz":
			p = "Suisse";
			return p;

		default:
			p = Minuscule(p);
			p = Majuscule(p);
			return p;

		}

	}

}
