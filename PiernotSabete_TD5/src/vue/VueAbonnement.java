package vue;

import java.io.IOException;
import java.net.URL;

import controleur.AfficherAbonnementControleur;
import factory.DAOFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class VueAbonnement extends Stage{
	
	public VueAbonnement(DAOFactory daos) throws IOException {
		final URL fxmlURL = getClass().getResource("/fxml/AfficherAbonnementFXML.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
		final AnchorPane node = (AnchorPane) fxmlLoader.load();
		Scene scene = new Scene(node);
		this.setScene(scene);
		this.setResizable(false);
		this.setWidth(900);
		this.setHeight(470);

		AfficherAbonnementControleur controleur = fxmlLoader.getController();
		controleur.setDaos(daos);
		controleur.initTabAbo();
		controleur.setVue(this);
		
		this.initModality(Modality.APPLICATION_MODAL);

		this.show();
	}
	
	
}
