package vue;

import java.io.IOException;
import java.net.URL;

import controleur.AfficherAbonnementControleur;
import controleur.AjouterAbonnementControleur;
import factory.DAOFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class VueAjouterAbonnement extends Stage{

	
	public VueAjouterAbonnement(DAOFactory daos, AfficherAbonnementControleur ctrl) throws IOException {
		final URL fxmlURL = getClass().getResource("/fxml/AjouterAbonnementFXML.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
		final AnchorPane node = (AnchorPane) fxmlLoader.load();
		Scene scene = new Scene(node);
		this.setScene(scene);
		this.setResizable(false);
		this.setWidth(660);
		this.setHeight(500);

		AjouterAbonnementControleur controleur = fxmlLoader.getController();
		controleur.setDaos(daos);
		controleur.initClient();
		controleur.initRevue();
		controleur.setVue(this);
		controleur.setCtrl(ctrl);

		this.initModality(Modality.APPLICATION_MODAL);

		this.show();
	}
	
}
