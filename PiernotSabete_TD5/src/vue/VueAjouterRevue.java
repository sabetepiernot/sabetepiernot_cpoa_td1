package vue;

import java.io.IOException;
import java.net.URL;

import controleur.AfficherRevueControleur;
import controleur.AjouterRevueControleur;
import factory.DAOFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class VueAjouterRevue extends Stage {

	public VueAjouterRevue(DAOFactory daos, AfficherRevueControleur ctrl) throws IOException {
		final URL fxmlURL = getClass().getResource("/fxml/AjouterRevueFXML.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
		final AnchorPane node = (AnchorPane) fxmlLoader.load();
		Scene scene = new Scene(node);
		this.setScene(scene);
		this.setResizable(false);
		this.setWidth(530);
		this.setHeight(567);

		AjouterRevueControleur controleur = fxmlLoader.getController();
		controleur.setDaos(daos);
		controleur.initPeriodicite();
		controleur.setVue(this);
		controleur.setCtrl(ctrl);

		this.initModality(Modality.APPLICATION_MODAL);

		this.show();
	}
}