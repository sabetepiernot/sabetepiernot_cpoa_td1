package vue;

import java.io.IOException;
import java.net.URL;

import controleur.AfficherClientControleur;
import factory.DAOFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class VueClient extends Stage {

	public VueClient(DAOFactory daos) throws IOException {
		final URL fxmlURL = getClass().getResource("/fxml/AfficherClientFXML.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
		final AnchorPane node = (AnchorPane) fxmlLoader.load();
		Scene scene = new Scene(node);
		this.setScene(scene);
		this.setResizable(false);
		this.setHeight(475);
		this.setWidth(900);
		
		AfficherClientControleur controleur = fxmlLoader.getController();
		controleur.setDaos(daos);
		controleur.initial();
		controleur.setVue(this);

		this.initModality(Modality.APPLICATION_MODAL);

		this.show();
	}
}
