package vue;

import java.io.IOException;
import java.net.URL;

import controleur.AfficherClientControleur;
import controleur.ModifierClientControleur;
import factory.DAOFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.Client;

public class VueModifierClient extends Stage{

	public VueModifierClient(DAOFactory daos, AfficherClientControleur ctrl, Client client) throws IOException{
		final URL fxmlURL = getClass().getResource("/fxml/ModifierClientFXML.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
		final AnchorPane node = (AnchorPane) fxmlLoader.load();
		Scene scene = new Scene(node);
		this.setScene(scene);
		this.setResizable(false);
		this.setWidth(660);
		
		ModifierClientControleur controleur = fxmlLoader.getController();
		controleur.setClient(client);
		controleur.initial();
		controleur.setDaos(daos);
		controleur.setVue(this);
		controleur.setCtrl(ctrl);
	

		this.initModality(Modality.APPLICATION_MODAL);

		this.show();	
	
	
	
	}}
