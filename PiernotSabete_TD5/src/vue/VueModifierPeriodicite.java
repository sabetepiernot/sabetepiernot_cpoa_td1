package vue;

import java.io.IOException;
import java.net.URL;

import controleur.AfficherPeriodiciteControleur;
import controleur.ModifierPeriodiciteControleur;
import factory.DAOFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.Periodicite;
import metier.Revue;

public class VueModifierPeriodicite extends Stage {

	public VueModifierPeriodicite(DAOFactory daos, AfficherPeriodiciteControleur ctrl, Periodicite periodicite) throws IOException {
		final URL fxmlURL = getClass().getResource("/fxml/ModifierPeriodiciteFXML.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
		final AnchorPane node = (AnchorPane) fxmlLoader.load();
		Scene scene = new Scene(node);
		this.setScene(scene);
		this.setResizable(false);
		this.setWidth(530);
		this.setHeight(330);

		ModifierPeriodiciteControleur controleur = fxmlLoader.getController();
		controleur.setPeriodicite(periodicite);
		controleur.initial();
		controleur.setDaos(daos);
		controleur.setVue(this);
		controleur.setCtrl(ctrl);

		this.initModality(Modality.APPLICATION_MODAL);

		this.show();
	}
}