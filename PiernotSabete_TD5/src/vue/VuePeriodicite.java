package vue;

import java.io.IOException;
import java.net.URL;

import controleur.AfficherPeriodiciteControleur;
import factory.DAOFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class VuePeriodicite extends Stage {
	

	public VuePeriodicite(DAOFactory daos) throws IOException {
		final URL fxmlURL = getClass().getResource("/fxml/AfficherPeriodiciteFXML.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
		final AnchorPane node = (AnchorPane) fxmlLoader.load();
		Scene scene = new Scene(node);
		this.setScene(scene);
		this.setResizable(false);

		
		AfficherPeriodiciteControleur controleur = fxmlLoader.getController();
		controleur.setDaos(daos);
		controleur.initial();
		controleur.setVue(this);

		this.initModality(Modality.APPLICATION_MODAL);

		this.show();
	}
}
