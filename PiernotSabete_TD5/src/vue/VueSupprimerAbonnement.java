package vue;

import java.io.IOException;
import java.net.URL;

import controleur.AfficherAbonnementControleur;
import controleur.SupprimerAbonnementControleur;
import factory.DAOFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.Abonnement;

public class VueSupprimerAbonnement extends Stage{

	public VueSupprimerAbonnement(DAOFactory daos, AfficherAbonnementControleur ctrl, Abonnement abo) throws IOException {
		final URL fxmlURL = getClass().getResource("/fxml/SupprimerAbonnementFXML.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
		final AnchorPane node = (AnchorPane) fxmlLoader.load();
		Scene scene = new Scene(node);
		this.setScene(scene);
		this.setResizable(false);
		this.setWidth(678);
		this.setHeight(340);
	

		SupprimerAbonnementControleur controleur = fxmlLoader.getController();
		controleur.setAbonnement(abo);
		controleur.initial();
		controleur.setDaos(daos);
		controleur.setVue(this);
		controleur.setCtrl(ctrl);

		this.initModality(Modality.APPLICATION_MODAL);

		this.show();
	}	
	
	
}
