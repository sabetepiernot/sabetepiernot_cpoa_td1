package vue;

import java.io.IOException;
import java.net.URL;

import controleur.AfficherClientControleur;
import controleur.SupprimerClientControleur;
import factory.DAOFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.Client;


public class VueSupprimerClient extends Stage{

	public VueSupprimerClient(DAOFactory daos, AfficherClientControleur ctrl, Client client) throws IOException {
		final URL fxmlURL = getClass().getResource("/fxml/SupprimerClientFXML.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
		final AnchorPane node = (AnchorPane) fxmlLoader.load();
		Scene scene = new Scene(node);
		this.setScene(scene);
		this.setResizable(false);
		this.setWidth(530);

		SupprimerClientControleur controleur = fxmlLoader.getController();
		controleur.setClient(client);
		controleur.initial();
		controleur.setDaos(daos);
		controleur.setVue(this);
		controleur.setCtrl(ctrl);

		this.initModality(Modality.APPLICATION_MODAL);

		this.show();
	}	
	
	
}
