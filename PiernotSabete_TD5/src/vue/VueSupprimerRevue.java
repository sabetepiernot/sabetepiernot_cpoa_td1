package vue;

import java.io.IOException;
import java.net.URL;

import controleur.AfficherRevueControleur;
import controleur.SupprimerRevueControleur;
import factory.DAOFactory;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import metier.Revue;

public class VueSupprimerRevue extends Stage {

	public VueSupprimerRevue(DAOFactory daos, AfficherRevueControleur ctrl, Revue revue) throws IOException {
		final URL fxmlURL = getClass().getResource("/fxml/SupprimerRevueFXML.fxml");
		final FXMLLoader fxmlLoader = new FXMLLoader(fxmlURL);
		final AnchorPane node = (AnchorPane) fxmlLoader.load();
		Scene scene = new Scene(node);
		this.setScene(scene);
		this.setResizable(false);
		this.setWidth(530);
		this.setHeight(339);

		SupprimerRevueControleur controleur = fxmlLoader.getController();
		controleur.setRevue(revue);
		controleur.initial();
		controleur.setDaos(daos);
		controleur.setVue(this);
		controleur.setCtrl(ctrl);

		this.initModality(Modality.APPLICATION_MODAL);

		this.show();
	}
}